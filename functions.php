<?php
/**
 * sassy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sassy
 */

/**
 * Define constants
 */
$theme_options  = wp_get_theme();
$theme_name     = $theme_options->get( 'Name' );
$theme_author   = $theme_options->get( 'Author' );
$theme_desc     = $theme_options->get( 'Description' );
$theme_version  = $theme_options->get( 'Version' );

define( 'THEME_NAME', $theme_name );
define( 'THEME_AUTHOR', $theme_author );
define( 'THEME_DESCRIPTION', $theme_desc );
define( 'THEME_VERSION', $theme_version );
define( 'THEME_URI', get_template_directory_uri() );
define( 'THEME_DIR', get_template_directory() );

if ( ! function_exists( 'sassy_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sassy_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sassy, use a find and replace
		 * to change 'sassy' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sassy', THEME_DIR . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'sassy' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

        /*
        * Add Excerpt for the Pages.
        */
        add_post_type_support( 'page', 'excerpt' );

        /*
        * Post Formats
        */
        add_theme_support( 'post-formats', array( 'gallery', 'link', 'quote', 'video', 'audio' ) );
	}
endif;
add_action( 'after_setup_theme', 'sassy_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sassy_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sassy_content_width', 640 );
}
add_action( 'after_setup_theme', 'sassy_content_width', 0 );

/*--------------------------------------------------------------
# Front-End Enqueue scripts and styles.
--------------------------------------------------------------*/
function sassy_scripts() {

    // Bootstrap
    wp_enqueue_style( 'bootstrap', THEME_URI .'/assets/front-end/css/bootstrap.min.css', false, '4.0.0', 'all' );

	wp_enqueue_style( 'sassy-style', get_stylesheet_uri() );

	wp_enqueue_script( 'sassy-navigation', THEME_URI . '/assets/front-end/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'sassy-skip-link-focus-fix', THEME_URI . '/assets/front-end/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sassy_scripts' );

/*--------------------------------------------------------------
# Back-End Enqueue scripts and styles.
--------------------------------------------------------------*/
if ( !function_exists( 'sassy_admin_scripts' ) ) {
    function sassy_admin_scripts() {

        // Get Current Screen Name
        $current_screen = get_current_screen();

        // Run some code, only on the customizer and widgets page
        if ( $current_screen->id == "customize" || $current_screen->id == "widgets" ) {

            wp_enqueue_style( 'sassy-customizer-style', THEME_URI .'/assets/back-end/css/customizer-style.css', false, THEME_VERSION, 'all' );
        }

    }
}
add_action( 'admin_enqueue_scripts', 'sassy_admin_scripts' );

/**
 * Implement theme custom meta box
 */
require THEME_DIR . '/inc/meta-boxes/class-meta-box.php';

/**
 * Implement the Custom Widgets and default Sidebar Area feature.
 */
require THEME_DIR . '/inc/widgets/widget-functions.php';

/**
 * Custom Sidebar Generator.
 */
require THEME_DIR . '/inc/widgets/sidebar-generator/class-sidebar-generator.php';

/**
 * Custom template tags for this theme.
 */
require THEME_DIR . '/inc/helpers/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require THEME_DIR . '/inc/helpers/template-functions.php';

/**
 * Functions which returns array for theme.
 */
require THEME_DIR . '/inc/helpers/template-array.php';

/**
 * Kirki Toolkit.
 */
require THEME_DIR . '/inc/libraries/kirki/kirki.php';

/**
 * Customizer additions.
 */
require THEME_DIR . '/inc/customizer/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require THEME_DIR . '/inc/libraries/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require THEME_DIR . '/inc/libraries/woocommerce.php';
}
