<?php
/**
 * Theme Customizer Blog Panel
 *
 * @package Sassy
 */
/*--------------------------------------------------------------
# Blog Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_archive_page_panel', array(
    'priority'      => 124,
    'title'         => esc_html__( 'Archive/Blog Settings', 'sassy' ),
));

/*--------------------------------------------------------------
# Header Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_header_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Archive Header', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Radio Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_archive_page_header_color_scheme',
    'label'       => esc_html__( 'Color Scheme', 'sassy' ),
    'section'     => 'sassy_archive_page_header_section',
    'default'     => 'dark',
    'choices'     => array(
        'dark'   => esc_html__( 'Dark', 'sassy' ),
        'light'  => esc_html__( 'Light', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Color Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'color',
    'settings'  =>  'sassy_archive_page_header_background_color',
    'section'   =>  'sassy_archive_page_header_section',
    'label'     =>  esc_html__( 'Background Color', 'sassy' ),
    'default'   =>  '#f4f8f9',
    'choices'   => array(
        'alpha' => false,
    ),
    'transport'     =>  'postMessage',
    'js_vars'       =>  array(
        array(
            'element'   =>  array( '.archive .page-header-content, .archive .woocommerce-products-header-content' ),
            'function'  =>  'css',
            'property'  =>  'background'
        )
    ),
    'output'        =>  array(
        array(
            'element'   =>  array( '.archive .page-header-content, .archive .woocommerce-products-header-content' ),
            'function'  =>  'css',
            'property'  =>  'background'
        )
    )
));

/*--------------------------------------------------------------
# Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'image',
    'settings'  =>  'sassy_archive_page_header_background_image',
    'section'   =>  'sassy_archive_page_header_section',
    'label'     =>  esc_html__( 'Background Image', 'sassy' ),
    'description' => esc_html__( 'The recommended size for the background image is 1360x320 pixels', 'sassy' ),
    'default'     => '',
    'choices'     => array(
        'save_as' => 'id',
    ),
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_header_text_alignment',
    'label'       => esc_html__( 'Text Alignment', 'sassy' ),
    'section'     => 'sassy_archive_page_header_section',
    'default'     => 'align-left',
    'choices'     => array(
        'align-left'    => esc_attr__( 'Left', 'sassy' ),
        'align-center'  => esc_attr__( 'Center', 'sassy' ),
        'align-right'   => esc_attr__( 'Right', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_layout_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Layout', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Radio-Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_archive_page_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'description' => esc_html__( 'Select the layout for the blog.', 'sassy' ),
    'section'     => 'sassy_archive_page_layout_section',
    'default'     => 'blog-layout-1',
    'choices'     => array(
        'blog-layout-1'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-1.svg',
        'blog-layout-2'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-2.svg',
        'blog-layout-3'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-3.svg',
        'blog-layout-4'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-4.svg',
        'blog-layout-5'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-5.svg',
        'blog-layout-6'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-6.svg',
        'blog-layout-7'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-7.svg',
        'blog-layout-8'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-8.svg',
        'blog-layout-9'         => THEME_URI . '/assets/back-end/images/blog/blog-layout-9.svg',
        'blog-layout-11'        => THEME_URI . '/assets/back-end/images/blog/blog-layout-11.svg'
    ),
) );

/*--------------------------------------------------------------
# Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_settings_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_thumbnail_ratio',
    'label'       => esc_html__( 'Thumbnail Ratio', 'sassy' ),
    'section'     => 'sassy_archive_page_settings_section',
    'default'     => '16x9',
    'choices'     => array(
        '16x9'          => esc_html__( '16 : 9 Ratio', 'sassy' ),
        '4x3'           => esc_html__( '4 : 3 Ratio', 'sassy' ),
        'auto'          => esc_html__( 'Auto Height', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_col_per_row',
    'label'       => esc_html__( 'Columns Per Row', 'sassy' ),
    'section'     => 'sassy_archive_page_settings_section',
    'default'     => 'col-4',
    'choices'     => array(
        'col-2'     => '2',
        'col-3'     => '3',
        'col-4'     => '4',
    ),
) );

/*--------------------------------------------------------------
# Sortable Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'sortable',
    'settings'    => 'sassy_blog_layout1_content_order_list',
    'label'       => esc_html__( 'Content Order', 'sassy' ),
    'description' => esc_html__( 'Drag & Drop items to re-arrange order of appearance. Note:- Please enable social share for post to work it through Appearance->Customize->Social->Social Share.', 'sassy' ),
    'section'     => 'sassy_archive_page_settings_section',
    'default'     => array(
        'post-featured-image',
        'post-title',
        'post-meta',
        'post-content',
    ),
    'choices'     => array(
        'post-featured-image'   => esc_attr__( 'Featured Image', 'sassy' ),
        'post-title'            => esc_attr__( 'Title', 'sassy' ),
        'post-meta'             => esc_attr__( 'Meta', 'sassy' ),
        'post-content'          => esc_attr__( 'Content', 'sassy' ),
        'post-social-share'     => esc_attr__( 'Social Share', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_layout',
            'operator' => '==',
            'value'    => 'post-layout-1',
        ),
    ),
) );

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_text_alignment',
    'label'       => esc_html__( 'Content Alignment', 'sassy' ),
    'section'     => 'sassy_archive_page_settings_section',
    'default'     => 'content-align-left',
    'choices'     => array(
        'content-align-left'     => esc_attr__( 'Left', 'sassy' ),
        'content-align-center'   => esc_attr__( 'Center', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Slide Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_archive_page_excerpt_length',
    'label'       => esc_html__( 'Excerpt Length', 'sassy' ),
    'section'     => 'sassy_archive_page_settings_section',
    'default'     => 18,
    'choices'     => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
) );
/*--------------------------------------------------------------
# Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_sidebar_section', array(
    'priority'      => 4,
    'title'         => esc_html__( 'Sidebar', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Radio Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_archive_page_global_sidebar',
    'label'       => esc_html__( 'Archive/Blog Sidebar', 'sassy' ),
    'description' => esc_html__( 'Select default sidebar. This sidebar will be reflected in whole site archives, categories, search page etc.', 'sassy' ),
    'section'     => 'sassy_archive_page_sidebar_section',
    'default'     => 'right-sidebar',
    'choices'     => array(
        'left-sidebar'      => THEME_URI . '/assets/back-end/images/sidebar/left-sidebar.svg',
        'full-width'        => THEME_URI . '/assets/back-end/images/sidebar/no-sidebar.svg',
        'right-sidebar'     => THEME_URI . '/assets/back-end/images/sidebar/right-sidebar.svg',

    ),
) );
/*--------------------------------------------------------------
# Meta Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_meta_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'Meta', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_blog_meta_date_activate',
    'label'         =>  esc_html__( 'Date', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show date meta on post.', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_date_label',
    'label'         =>  esc_html__( 'Date Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Published on: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_date_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-date .date-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_blog_meta_author_activate',
    'label'         =>  esc_html__( 'Author', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show author meta on post.', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_author_activate',
    'label'         =>  esc_html__( 'Author Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Author: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_author_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-author .author-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_blog_meta_comment_activate',
    'label'         =>  esc_html__( 'Comments', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show comments meta on post.', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_comment_label',
    'label'         =>  esc_html__( 'Comment Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Comment: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_comment_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-comment .comment-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_blog_meta_category_activate',
    'label'         =>  esc_html__( 'Categories', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show categories meta on post.', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_category_label',
    'label'         =>  esc_html__( 'Category Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Category:', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_category_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-category .category-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_blog_meta_tags_activate',
    'label'         =>  esc_html__( 'Tags', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show tags meta on post.', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_tags_label',
    'label'         =>  esc_html__( 'Tag Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Tag:', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_tags_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-tag .tag-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_blog_meta_like_system_activate',
    'section'   =>  'sassy_archive_page_meta_section',
    'label'     =>  esc_html__( 'Like', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to show post like system on post.', 'sassy' ),
    'default'   =>  1
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_blog_meta_like_system_label',
    'label'         =>  esc_html__( 'Like Label', 'sassy' ),
    'section'       =>  'sassy_archive_page_meta_section',
    'default'       =>  esc_html__( 'Like: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_blog_meta_like_system_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-like .like-label',
            'function' => 'html',
        ),
    ),
));


/*--------------------------------------------------------------
# Read More Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_read_more_section', array(
    'priority'      => 6,
    'title'         => esc_html__( 'Read More', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_archive_page_read_more_activate',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display read more button.', 'sassy' ),
    'section'       =>  'sassy_archive_page_read_more_section',
    'default'       =>  '1',
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_archive_page_read_more_text',
    'label'         =>  esc_html__( 'Text', 'sassy' ),
    'section'       =>  'sassy_archive_page_read_more_section',
    'default'       =>  esc_html__( 'Discover', 'sassy' ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.blog-layout .entry-footer a',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_read_more_button_type',
    'label'       => esc_html__( 'Type', 'sassy' ),
    'section'     => 'sassy_archive_page_read_more_section',
    'default'     => 'arrow',
    'choices'     => array(
        'rounded'       => esc_attr__( 'Rounded', 'sassy' ),
        'arrow'         => esc_attr__( 'Arrow', 'sassy' ),
        'underline'     => esc_attr__( 'Underline', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_archive_page_read_more_button_border_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_archive_page_read_more_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '30',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_read_more_button_type',
            'operator' => '==',
            'value'    => 'rounded',
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.blog-layout .entry-footer a.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.blog-layout .entry-footer a.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_archive_page_read_more_button_transparency',
    'label'       => esc_html__( 'Background Transparent', 'sassy' ),
    'section'     => 'sassy_archive_page_read_more_section',
    'default'     => '1',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_read_more_button_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
    ),
) );

/*--------------------------------------------------------------
# Radio Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_archive_page_read_more_button_color_scheme',
    'label'       => esc_html__( 'Scheme', 'sassy' ),
    'section'     => 'sassy_archive_page_read_more_section',
    'default'     => 'light',
    'choices'     => array(
        'dark'   => esc_html__( 'Dark', 'sassy' ),
        'light'  => esc_html__( 'Light', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_read_more_button_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
    ),
) );

/*--------------------------------------------------------------
# Pagination Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_archive_page_pagination_section', array(
    'priority'      => 7,
    'title'         => esc_html__( 'Pagination', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_pagination_type',
    'label'       => esc_html__( 'Pagination Type', 'sassy' ),
    'section'     => 'sassy_archive_page_pagination_section',
    'default'     => 'pagination_post_number',
    'choices'     => array(
        'pagination_post_number'     => esc_attr__( 'Post Number', 'sassy' ),
        'pagination_post_on_click'   => esc_attr__( 'Post Load onClick', 'sassy' ),
        'pagination_post_on_scroll'  => esc_attr__( 'Post Load onScroll', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
) );

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_pagination_alignment',
    'label'       => esc_html__( 'Pagination Alignment', 'sassy' ),
    'section'     => 'sassy_archive_page_pagination_section',
    'default'     => 'left',
    'choices'     => array(
        'left'          => esc_attr__( 'Left', 'sassy' ),
        'center'        => esc_attr__( 'Center', 'sassy' ),
        'right'         => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
) );

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_archive_page_pagination_load_more_text',
    'label'         =>  esc_html__( 'Load More Text', 'sassy' ),
    'section'       =>  'sassy_archive_page_pagination_section',
    'default'       =>  esc_html__( 'Load More', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_pagination_type',
            'operator' => '!=',
            'value'    => 'pagination_post_number',
        ),
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.load-more-posts-btn',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_archive_page_pagination_no_more_post_text',
    'label'         =>  esc_html__( 'No More Post Text', 'sassy' ),
    'section'       =>  'sassy_archive_page_pagination_section',
    'default'       =>  esc_html__( 'No More Post', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_pagination_type',
            'operator' => '!=',
            'value'    => 'pagination_post_number',
        ),
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
    'transport' => 'postMessage',
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_archive_page_pagination_button_type',
    'label'       => esc_html__( 'Button Type', 'sassy' ),
    'section'     => 'sassy_archive_page_pagination_section',
    'default'     => 'arrow',
    'choices'     => array(
        'rounded'       => esc_attr__( 'Rounded', 'sassy' ),
        'arrow'         => esc_attr__( 'Arrow', 'sassy' ),
        'underline'     => esc_attr__( 'Underline', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_pagination_type',
            'operator' => '!=',
            'value'    => 'pagination_post_number',
        ),
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_archive_page_pagination_button_border_radius',
    'label'       => esc_attr__( 'Button Radius', 'sassy' ),
    'section'     => 'sassy_archive_page_pagination_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '30',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_pagination_button_type',
            'operator' => '==',
            'value'    => 'rounded',
        ),
        array(
            'setting'  => 'sassy_archive_page_pagination_type',
            'operator' => '!=',
            'value'    => 'pagination_post_number',
        ),
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.load-more-posts-btn.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.load-more-posts-btn.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );


/*--------------------------------------------------------------
# Radio Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_archive_page_pagination_button_color_scheme',
    'label'       => esc_html__( 'Button Scheme', 'sassy' ),
    'section'     => 'sassy_archive_page_pagination_section',
    'default'     => 'light',
    'choices'     => array(
        'dark'   => esc_html__( 'Dark', 'sassy' ),
        'light'  => esc_html__( 'Light', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_archive_page_pagination_button_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
        array(
            'setting'  => 'sassy_archive_page_pagination_type',
            'operator' => '!=',
            'value'    => 'pagination_post_number',
        ),
        array(
            'setting'  => 'sassy_archive_page_layout',
            'operator' => '!=',
            'value'    => 'blog-layout-11',
        ),
    ),
) );

/*--------------------------------------------------------------
# Widget Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_blog_widgets_section', array(
    'priority'      => 8,
    'title'         => esc_html__( 'Widgets', 'sassy' ),
    'panel'         => 'sassy_archive_page_panel',
    'capability'    => 'edit_theme_options',
));

