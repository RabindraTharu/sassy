<?php
/**
 * Theme Customizer 404 Page Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# 404 Error Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_404_error_page_panel', array(
    'priority'      => 122,
    'title'         => esc_html__( '404 Error Page', 'sassy' ),
));

/*--------------------------------------------------------------
# 404 Error Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_404_error_page_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Page Settings', 'sassy' ),
    'panel'         => 'sassy_404_error_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Text alignment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_404_error_page_text_alignment',
    'label'       => esc_html__( 'Content Alignment', 'sassy' ),
    'section'     => 'sassy_404_error_page_section',
    'default'     => 'align-left',
    'choices'     => array(
        'align-left'     => esc_attr__( 'Left', 'sassy' ),
        'align-center'   => esc_attr__( 'Center', 'sassy' ),
        'align-right'    => esc_attr__( 'Right', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Error Page Title Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_404_error_page_title',
    'label'         =>  esc_html__( 'Title', 'sassy' ),
    'section'       =>  'sassy_404_error_page_section',
    'default'       =>  __( 'Oops! That page can\'t be found.', 'sassy' ),
));

/*--------------------------------------------------------------
# Error Page Description Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'textarea',
    'settings'      =>  'sassy_404_error_page_description',
    'label'         =>  esc_html__( 'Short Description', 'sassy' ),
    'section'       =>  'sassy_404_error_page_section',
    'default'       =>  esc_html__( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'sassy' ),
));

/*--------------------------------------------------------------
# Error Page Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'image',
    'settings'      =>  'sassy_404_error_page_image',
    'label'         =>  esc_html__( 'Error Image', 'sassy' ),
    'description'   => esc_html__( 'The recommended size for the 404 image is any suitable size, not bigger than 1200px wide.', 'sassy' ),
    'section'       =>  'sassy_404_error_page_section',
));

/*--------------------------------------------------------------
# Activate Back to Home Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_home_link_activate',
    'label'     =>  esc_html__( 'Back to Home Button', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
    'default'   =>  '1',
));

/*--------------------------------------------------------------
# Text Back to Home Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'text',
    'settings'  =>  'sassy_404_error_page_home_button_text',
    'label'     =>  esc_html__( 'Button Text', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
    'default'   =>  esc_html__( 'Back to Home', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_404_error_page_home_link_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Search Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_search_activate',
    'label'     =>  esc_html__( 'Search Bar', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
    'default'   =>  '1',
));

/*--------------------------------------------------------------
# Activate Recent Posts Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_recent_post_activate',
    'label'     =>  esc_html__( 'Recent Post', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
));

/*--------------------------------------------------------------
# Activate Tags Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_tags_activate',
    'label'     =>  esc_html__( 'Tag', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
));

/*--------------------------------------------------------------
# Activate Categories Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_categories_activate',
    'label'     =>  esc_html__( 'Categories', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
));

/*--------------------------------------------------------------
# Activate Archives Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_404_error_page_archives_activate',
    'label'     =>  esc_html__( 'Archives', 'sassy' ),
    'section'   =>  'sassy_404_error_page_section',
));

/*--------------------------------------------------------------
# 404 Error Page Widgets area Section
--------------------------------------------------------------*/
Kirki::add_section('sassy_404_error_page_widgets_section', array(
    'priority'      =>  2,
    'title'         => esc_html__( 'Widgets', 'sassy' ),
    'panel'         => 'sassy_404_error_page_panel',
    'capability'    => 'edit_theme_options',
));
