<?php
/**
 * Theme Customizer Post Panel
 *
 * @package Sassy
 */
/*--------------------------------------------------------------
# Footer Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_footer_panel', array(
    'priority'      => 126,
    'title'         => esc_html__( 'Footer', 'sassy' ),
));

/*--------------------------------------------------------------
# Back To Top Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_footer_back_to_top_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Back to Top', 'sassy' ),
    'panel'         => 'sassy_footer_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Back to Top Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_footer_back_to_top_activate',
    'label'     =>  esc_html__( 'Activate Button', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to display back to top button.', 'sassy' ),
    'section'   =>  'sassy_footer_back_to_top_section',
    'default'   =>  '1',
));

/*--------------------------------------------------------------
# Back to Top Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'text',
    'settings'  =>  'sassy_footer_back_to_top_text',
    'label'     =>  esc_html__( 'Button Text', 'sassy' ),
    'section'   =>  'sassy_footer_back_to_top_section',
    'default'   =>  esc_html__( 'Back to Top', 'sassy' ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.back-to-top',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Footer Widget Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_footer_widgets_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Footer Widgets', 'sassy' ),
    'panel'         => 'sassy_footer_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Footer Widgets Setting & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_footer_widgets_section_activate',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display Footer Widget Area on all Pages.', 'sassy' ),
    'section'       =>  'sassy_footer_widgets_section',
    'default'       =>  '1',
));

/*--------------------------------------------------------------
# Layout Setting & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_footer_widgets_area_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'description' => esc_html__( 'Select layout for footer widget columns. It generates some widget areas for Footer based on the layout.', 'sassy' ),
    'section'     => 'sassy_footer_widgets_section',
    'default'     => 'footer-layout-8',
    'choices'     => array(
        'footer-layout-1'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-1.svg',
        'footer-layout-2'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-2.svg',
        'footer-layout-3'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-3.svg',
        'footer-layout-4'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-4.svg',
        'footer-layout-5'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-5.svg',
        'footer-layout-6'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-6.svg',
        'footer-layout-7'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-7.svg',
        'footer-layout-8'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-8.svg',
        'footer-layout-9'           => THEME_URI . '/assets/back-end/images/footer/footer-layout-9.svg',
        'footer-layout-10'          => THEME_URI . '/assets/back-end/images/footer/footer-layout-10.svg'
    ),
) );

/*--------------------------------------------------------------
# Footer Bar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_footer_bar_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Footer Bar', 'sassy' ),
    'panel'         => 'sassy_footer_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Footer Bar Content Ordering Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'sortable',
    'settings'    => 'sassy_footer_bar_content_order_list',
    'label'       => esc_html__( 'Content Order', 'sassy' ),
    'description' => esc_html__( 'Drag & Drop items to re-arrange order of appearance.', 'sassy' ),
    'section'     => 'sassy_footer_bar_section',
    'default'     => array(
        'footer-bar-text',
    ),
    'choices'     => array(
        'footer-bar-text'       => esc_attr__( 'Copyright Text', 'sassy' ),
        'footer-bar-menu'       => esc_attr__( 'Footer Menu', 'sassy' ),
        'footer-bar-social'     => esc_attr__( 'Social Icons', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Rooter Text alignment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_footer_bar_content_text_alignment',
    'label'       => esc_html__( 'Text Alignment', 'sassy' ),
    'section'     => 'sassy_footer_bar_section',
    'default'     => 'align-center',
    'choices'     => array(
        'align-left'    => esc_attr__( 'Left', 'sassy' ),
        'align-center'  => esc_attr__( 'Center', 'sassy' ),
        'align-right'   => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback' => 'sassy_footer_bar_text_alignment_callback'
) );

function sassy_footer_bar_text_alignment_callback() {
    $order_list = get_theme_mod( 'sassy_footer_bar_content_order_list' );
    $count      = count( $order_list );
    if ( $count == 1 ) {
        return true;
    } else {
        return false;
    }
}
/*--------------------------------------------------------------
# Footer Separator Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_footer_separator_section', array(
    'priority'      =>  4,
    'title'         => esc_html__( 'Footer Bar Separator', 'sassy' ),
    'panel'         => 'sassy_footer_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Footer Separator Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_footer_separator_activate',
    'section'       =>  'sassy_footer_separator_section',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display footer separator line in between footer widgets & footer bar content section.', 'sassy' ),
    'default'       =>  '1',
));

/*--------------------------------------------------------------
# Footer Copyright Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_footer_copyright_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'Footer Copyright', 'sassy' ),
    'panel'         => 'sassy_footer_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Copyright Setting & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'editor',
    'settings'      =>  'sassy_footer_copyright_text',
    'label'         => esc_html__( 'Copyrights Text', 'sassy' ),
    'section'       =>  'sassy_footer_copyright_section',
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'  => '#colophon .footer-copyright',
            'function' => 'html',
        ),
    ),
));
