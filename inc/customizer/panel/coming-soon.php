<?php
/**
 * Theme Customizer Coming Soon Panel
 *
 * @package Sassy
 */
/*--------------------------------------------------------------
# Coming Soon Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_coming_soon_panel', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Coming Soon', 'sassy' ),
));

/*--------------------------------------------------------------
# Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_coming_soon_settings_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_coming_soon_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Coming Soon Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_coming_soon_activate',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'section'   =>  'sassy_coming_soon_settings_section',
));

/*--------------------------------------------------------------
# Logo Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'image',
    'settings'  =>  'sassy_coming_soon_logo',
    'label'     =>  esc_html__( 'Logo', 'sassy' ),
    'description' => esc_html__( 'The recommended size for the logo is 400x300 pixels', 'sassy' ),
    'section'   =>  'sassy_coming_soon_settings_section',
));
/*--------------------------------------------------------------
# Title Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'text',
    'settings'  =>  'sassy_coming_soon_title',
    'label'     =>  esc_html__( 'Title', 'sassy' ),
    'section'   =>  'sassy_coming_soon_settings_section',
    'default'   =>  esc_html__( 'Hello,', 'sassy' )
));

/*--------------------------------------------------------------
# Description Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'textarea',
    'settings'  =>  'sassy_coming_soon_description',
    'label'     =>  esc_html__( 'Short Description', 'sassy' ),
    'section'   =>  'sassy_coming_soon_settings_section',
    'default'   =>  esc_html__( 'Currently, we are working hard to complete our awesome design and planning to come up with magnificent website. Stay tuned, we are launching soon.', 'sassy' )
));

/*--------------------------------------------------------------
# Background Color Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'color',
    'settings'  =>  'sassy_coming_soon_background_color',
    'section'   =>  'sassy_coming_soon_settings_section',
    'label'     =>  esc_html__( 'Background Color', 'sassy' ),
    'default'   =>  '#f4f4f5',
    'choices'   => array(
        'alpha' => false,
    ),
));

/*--------------------------------------------------------------
# Background Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'image',
    'settings'  =>  'sassy_coming_soon_background_image',
    'section'   =>  'sassy_coming_soon_settings_section',
    'label'     =>  esc_html__( 'Background Image', 'sassy' ),
    'description' => esc_html__( 'The recommended size for the background image is 1920x1000 pixels', 'sassy' ),
));

/*--------------------------------------------------------------
# Powered By Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_coming_soon_powered_by',
    'label'       => esc_html__( 'Show Author Credits', 'sassy' ),
    'section'     => 'sassy_coming_soon_settings_section',
    'default'     => 'yes',
    'choices'     => array(
        'yes'   => esc_html__('Yes - we love you', 'sassy'),
        'no'    => esc_html__('No', 'sassy'),
    ),
) );
/*--------------------------------------------------------------
# Social Profiles Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_coming_soon_social_section', array(
    'priority'      =>  2,
    'title'         =>  esc_html__( 'Social Profiles', 'sassy' ),
    'panel'         => 'sassy_coming_soon_panel',
    'capability'    => 'edit_theme_options',

));

/*--------------------------------------------------------------
# Activate Social Profiles & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'toggle',
    'settings'  =>  'sassy_coming_soon_social_activate',
    'section'   =>  'sassy_coming_soon_social_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'You can add Social Profiles through Appearance -> Customize -> Social -> Social Profiles.', 'sassy' ),
));

/*--------------------------------------------------------------
# Timer Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_coming_soon_timer_section', array(
    'priority'      =>  3,
    'title'         =>  esc_html__( 'Countdown Timer', 'sassy' ),
    'panel'         => 'sassy_coming_soon_panel',
    'capability'    => 'edit_theme_options',

));

/*--------------------------------------------------------------
# Activate Timer Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'toggle',
    'settings'  =>  'sassy_coming_soon_timer_activate',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
));

/*--------------------------------------------------------------
# Date Picker Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'date',
    'settings'  =>  'sassy_coming_soon_timer_lunch_date',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Launch Date', 'sassy' ),
));

/*--------------------------------------------------------------
# Days Label Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'text',
    'settings'  =>  'sassy_coming_soon_timer_lunch_date_days_label',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Label for Day', 'sassy' ),
    'default'   =>  esc_html__( 'Days', 'sassy' ),
));

/*--------------------------------------------------------------
# Hours Label Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'text',
    'settings'  =>  'sassy_coming_soon_timer_lunch_date_hours_label',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Label for Hour', 'sassy' ),
    'default'   =>  esc_html__( 'Hrs', 'sassy' ),
));

/*--------------------------------------------------------------
# Minute Label Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'text',
    'settings'  =>  'sassy_coming_soon_timer_lunch_date_minute_label',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Label for Minute', 'sassy' ),
    'default'   =>  esc_html__( 'Min', 'sassy' ),
));

/*--------------------------------------------------------------
# Second Label Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      => 'text',
    'settings'  =>  'sassy_coming_soon_timer_lunch_date_second_label',
    'section'   =>  'sassy_coming_soon_timer_section',
    'label'     =>  esc_html__( 'Label for Second', 'sassy' ),
    'default'   =>  esc_html__( 'Sec', 'sassy' ),
));

/*--------------------------------------------------------------
# Coming Soon Section
--------------------------------------------------------------*/
Kirki::add_section('sassy_coming_soon_page_subscribe_section', array(
    'priority'      =>  4,
    'title'         =>  esc_html__( 'Email Subscribe', 'sassy' ),
    'panel'         => 'sassy_coming_soon_panel',
    'capability'    => 'edit_theme_options',
));