<?php
/**
 * Envy Blog Customizer Themes Info Section
 *
 * @package Envy Blog Pro
 */

/*--------------------------------------------------------------
# Themes Info Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_theme_info_section', array(
    'title'          => esc_html__( 'Themes Info', 'sassy' ),
    'priority'       => 200,
    'capability'     => 'edit_theme_options',
) );

/*--------------------------------------------------------------
# Themes Info Section
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'custom',
    'settings'    => 'sassy_theme_info_theme_link',
    'section'     => 'sassy_theme_info_section',
    'default'     => '<a target="_blank" href="' . esc_url( 'https://precisethemes.com/sassy/' ) . '">'.esc_html( 'Theme Info', 'sassy' ).'</a>',
) );

Kirki::add_field( 'sassy_config', array(
    'type'        => 'custom',
    'settings'    => 'sassy_theme_info_support_link',
    'section'     => 'sassy_theme_info_section',
    'default'     => '<a target="_blank" href="' . esc_url( 'https://precisethemes.com/support/' ) . '">'.esc_html( 'Support', 'sassy' ).'</a>',
) );

Kirki::add_field( 'sassy_config', array(
    'type'        => 'custom',
    'settings'    => 'sassy_theme_info_docs_link',
    'section'     => 'sassy_theme_info_section',
    'default'     => '<a target="_blank" href="' . esc_url( 'https://precisethemes.com/sassy-documentation/' ) . '">'.esc_html( 'Documentation', 'sassy' ).'</a>',
) );

Kirki::add_field( 'sassy_config', array(
    'type'        => 'custom',
    'settings'    => 'sassy_theme_info_demo_link',
    'section'     => 'sassy_theme_info_section',
    'default'     => '<a target="_blank" href="' . esc_url( 'http://demo.precisethemes.com/sassy/' ) . '">'.esc_html( 'View Demos', 'sassy' ).'</a>',
) );