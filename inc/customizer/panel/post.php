<?php
/**
 * Theme Customizer Post Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Post Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_post_panel', array(
    'priority'      => 123,
    'title'         => esc_html__( 'Post Settings', 'sassy' ),
));

/*--------------------------------------------------------------
# Breadcrumbs Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_breadcrumbs_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Breadcrumbs', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_post_breadcrumbs_activate',
    'section'   =>  'sassy_post_breadcrumbs_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to show breadcrumbs in all posts.', 'sassy' ),
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'text',
    'settings'  =>  'sassy_post_breadcrumbs_delimiter',
    'section'   =>  'sassy_post_breadcrumbs_section',
    'label'     =>  esc_html__( 'Delimiter', 'sassy' ),
    'default'   => '/',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_breadcrumbs_margin_y',
    'label'       => esc_attr__( 'Margin - Y', 'sassy' ),
    'section'     => 'sassy_post_breadcrumbs_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '15',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_breadcrumbs_spacing',
    'label'       => esc_attr__( 'Spacing', 'sassy' ),
    'section'     => 'sassy_post_breadcrumbs_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '15',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
) );

/*--------------------------------------------------------------
# Layout Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_layout_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Layout', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Single Post Layout Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_post_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'description' => esc_html__( 'Choose your single post page layout.', 'sassy' ),
    'section'     => 'sassy_post_layout_section',
    'default'     => 'post-layout-1',
    'choices'     => array(
        'post-layout-1'         => THEME_URI . '/assets/back-end/images/post/post-layout-1.svg',
        'post-layout-2'         => THEME_URI . '/assets/back-end/images/post/post-layout-2.svg',
    ),
) );

/*--------------------------------------------------------------
# Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_settings_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Content Ordering layout 1 Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'sortable',
    'settings'    => 'sassy_post_layout1_content_order_list',
    'label'       => esc_html__( 'Content Order', 'sassy' ),
    'description' => esc_html__( 'Drag & Drop items to re-arrange order of appearance. Note:- Please enable social share for post to work it through Appearance->Customize->Social->Social Share.', 'sassy' ),
    'section'     => 'sassy_post_settings_section',
    'default'     => array(
        'post-featured-image',
        'post-title',
        'post-meta',
        'post-content',
    ),
    'choices'     => array(
        'post-featured-image'   => esc_attr__( 'Featured Image', 'sassy' ),
        'post-title'            => esc_attr__( 'Title', 'sassy' ),
        'post-meta'             => esc_attr__( 'Meta', 'sassy' ),
        'post-content'          => esc_attr__( 'Content', 'sassy' ),
        'post-social-share'     => esc_attr__( 'Social Share', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_layout',
            'operator' => '==',
            'value'    => 'post-layout-1',
        ),
    ),
) );

/*--------------------------------------------------------------
# Text alignment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_post_content_text_alignment',
    'label'       => esc_html__( 'Content Alignment', 'sassy' ),
    'section'     => 'sassy_post_settings_section',
    'default'     => 'align-left',
    'choices'     => array(
        'align-left'     => esc_attr__( 'Left', 'sassy' ),
        'align-center'   => esc_attr__( 'Center', 'sassy' ),
        'align-right'    => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_layout',
            'operator' => '!=',
            'value'    => 'post-layout-2',
        ),
    ),
) );

/*--------------------------------------------------------------
# Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_sidebar_section', array(
    'priority'      => 4,
    'title'         => esc_html__( 'Sidebar', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Global Post Sidebar Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_post_global_sidebar',
    'label'       => esc_html__( 'Post Sidebar', 'sassy' ),
    'description' => esc_html__( 'Select default layout for single posts. This layout will be reflected in all single posts unless unique layout is set for specific post.', 'sassy' ),
    'section'     => 'sassy_post_sidebar_section',
    'default'     => 'right-sidebar',
    'choices'     => array(
        'left-sidebar'      => THEME_URI . '/assets/back-end/images/sidebar/left-sidebar.svg',
        'full-width'        => THEME_URI . '/assets/back-end/images/sidebar/no-sidebar.svg',
        'right-sidebar'     => THEME_URI . '/assets/back-end/images/sidebar/right-sidebar.svg',
    ),
) );

/*--------------------------------------------------------------
# Meta Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_meta_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'Meta', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Date Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_meta_date_activate',
    'label'         =>  esc_html__( 'Date', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show date meta on post.', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Label Text of Date Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_date_label',
    'label'         =>  esc_html__( 'Date Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Published on: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_date_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-date .date-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Author Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_meta_author_activate',
    'label'         =>  esc_html__( 'Author', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show author meta on post.', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Label Text of Author Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_author_activate',
    'label'         =>  esc_html__( 'Author Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Author: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_author_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-author .author-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Comment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_meta_comment_activate',
    'label'         =>  esc_html__( 'Comments', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show comments meta on post.', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Singular Comment label text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_comment_label',
    'label'         =>  esc_html__( 'Comment Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Comment: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_comment_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-comment .comment-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Category Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_meta_category_activate',
    'label'         =>  esc_html__( 'Categories', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show categories meta on post.', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Category label text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_category_label',
    'label'         =>  esc_html__( 'Category Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Category:', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_category_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-category .category-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Tags Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_meta_tags_activate',
    'label'         =>  esc_html__( 'Tags', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show tags meta on post.', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  1,
));

/*--------------------------------------------------------------
# Singular Tag label text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_tags_label',
    'label'         =>  esc_html__( 'Tag Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Tag:', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_tags_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-tag .tag-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Activate Like Meta Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_post_meta_like_system_activate',
    'section'   =>  'sassy_post_meta_section',
    'label'     =>  esc_html__( 'Like', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to show post like system on post.', 'sassy' ),
    'default'   =>  1
));

/*--------------------------------------------------------------
# Like label text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_meta_like_system_label',
    'label'         =>  esc_html__( 'Like Label', 'sassy' ),
    'section'       =>  'sassy_post_meta_section',
    'default'       =>  esc_html__( 'Like: ', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_meta_like_system_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single-post .entry-meta .posted-like .like-label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Navigation Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_navigation_section', array(
    'priority'      => 6,
    'title'         => esc_html__( 'Navigation', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Next/Prev Navigation Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_navigation_activate',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display post navigation in the post page.', 'sassy' ),
    'section'       =>  'sassy_post_navigation_section',
    'default'       =>  '1',
));

/*--------------------------------------------------------------
# Single Post Navigation Layout Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_post_navigation_layout',
    'label'       => esc_html__( 'Navigation Layout', 'sassy' ),
    'section'     => 'sassy_post_navigation_section',
    'default'     => 'navigation-layout-2',
    'choices'     => array(
        'navigation-layout-1'         => THEME_URI . '/assets/back-end/images/post/post-navigation/post-navigation-layout-1.svg',
        'navigation-layout-2'         => THEME_URI . '/assets/back-end/images/post/post-navigation/post-navigation-layout-2.svg',
        'navigation-layout-3'         => THEME_URI . '/assets/back-end/images/post/post-navigation/post-navigation-layout-3.svg',
    ),
) );

/*--------------------------------------------------------------
# Image border Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_navigation_image_border_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_post_navigation_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_navigation_layout',
            'operator' => '==',
            'value'    => 'navigation-layout-3',
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.post-navigation .entry-header .navigation-thumbnail img',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.post-navigation .entry-header .navigation-thumbnail img',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );
/*--------------------------------------------------------------
# Author Bio Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_author_section', array(
    'priority'      => 7,
    'title'         => esc_html__( 'Author Info Box', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Author Bio Section Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_author_activate',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display Author Bio Section after post content.', 'sassy' ),
    'section'       =>  'sassy_post_author_section',
));

/*--------------------------------------------------------------
# Author Avatar border radius Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_author_avatar_border_radius',
    'label'       => esc_attr__( 'Rounded Avatar', 'sassy' ),
    'section'     => 'sassy_post_author_section',
    'default'     => 2,
    'choices'     => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_author_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport'        =>  'postMessage',
    'js_vars' => array(
        array(
            'element'  => '.post-content .author-avatar img',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.post-content .author-avatar img',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );

/*--------------------------------------------------------------
# Activate Author Socials Section Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_author_socials_activate',
    'label'         =>  esc_html__( 'Socials Profiles', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display author socials in author section.', 'sassy' ),
    'section'       =>  'sassy_post_author_section',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_author_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
));

/*--------------------------------------------------------------
# Related Post Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_post_related_posts_section', array(
    'priority'      => 8,
    'title'         => esc_html__( 'Related Posts', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Related Posts Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_related_posts_activate',
    'label'         =>  esc_html__( 'Activate', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display Related Posts Section after post content.', 'sassy' ),
    'section'       =>  'sassy_post_related_posts_section',
));

/*--------------------------------------------------------------
# Related Block Title Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_related_posts_block_title',
    'label'         =>  esc_html__( 'Title', 'sassy' ),
    'section'       =>  'sassy_post_related_posts_section',
    'default'       =>  esc_html__( 'Related Posts', 'sassy' ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'  => '#primary .related-posts .related-posts-title',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Related Post query Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_post_related_posts_types',
    'label'       => esc_html__( 'Posts Query', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => 'same-category',
    'choices'     => array(
        'same-category'   => esc_html__( 'From Same Category', 'sassy' ),
        'same-tag'        => esc_html__( 'From Same Tag', 'sassy' ),
        'same-author'     => esc_html__( 'From Same Author', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Related Post Style Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_post_related_posts_col_per_row',
    'label'       => esc_html__( 'Column Per Row', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => '3',
    'choices'     => array(
        '2'  => '2',
        '3'  => '3',
        '4'  => '4',
    ),
) );

/*--------------------------------------------------------------
# Excerpt Length Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_related_posts_excerpt_length',
    'label'       => esc_html__( 'Excerpt Length', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => 18,
    'choices'     => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
) );

/*--------------------------------------------------------------
# Activate button Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_post_related_posts_read_more_activate',
    'section'       =>  'sassy_post_related_posts_section',
    'label'         =>  esc_html__( 'Read More', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to show the read more button.', 'sassy' ),
    'default'       => '1'
));

/*--------------------------------------------------------------
# Read More Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_post_related_posts_read_more_text',
    'label'         =>  esc_html__( 'Text', 'sassy' ),
    'section'       =>  'sassy_post_related_posts_section',
    'default'       =>  esc_html__( 'Discover', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_related_posts_read_more_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.single .related-posts .entry-footer a',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Read More Button Type Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_post_related_posts_read_more_type',
    'label'       => esc_html__( 'Type', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => 'arrow',
    'choices'     => array(
        'rounded'       => esc_attr__( 'Rounded', 'sassy' ),
        'arrow'         => esc_attr__( 'Arrow', 'sassy' ),
        'underline'     => esc_attr__( 'Underline', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_related_posts_read_more_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );

/*--------------------------------------------------------------
# Button border Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_post_related_posts_read_more_button_border_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '30',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_related_posts_read_more_type',
            'operator' => '==',
            'value'    => 'rounded',
        ),
        array(
            'setting'  => 'sassy_post_related_posts_read_more_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.single .related-posts .entry-footer a.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.single .related-posts .entry-footer a.rounded',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );

/*--------------------------------------------------------------
# Button Transparency Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_post_related_posts_read_more_button_transparency',
    'label'       => esc_html__( 'Background Transparent', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => '1',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_related_posts_read_more_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
        array(
            'setting'  => 'sassy_post_related_posts_read_more_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );

/*--------------------------------------------------------------
# Button Scheme Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_post_related_posts_read_more_button_scheme',
    'label'       => esc_html__( 'Scheme', 'sassy' ),
    'section'     => 'sassy_post_related_posts_section',
    'default'     => 'light',
    'choices'     => array(
        'dark'   => esc_html__( 'Dark', 'sassy' ),
        'light'  => esc_html__( 'Light', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_post_related_posts_read_more_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
        array(
            'setting'  => 'sassy_post_related_posts_read_more_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );

/*--------------------------------------------------------------
# Post Widgets area Section
--------------------------------------------------------------*/
Kirki::add_section('sassy_post_widgets_section', array(
    'priority'      =>  9,
    'title'         => esc_html__( 'Widgets', 'sassy' ),
    'panel'         => 'sassy_post_panel',
    'capability'    => 'edit_theme_options',
));
