<?php
/**
 * Theme Customizer Single Archive Panel
 *
 * @package Sassy
 */

$category_lists = get_terms( 'category', array(
    'orderby'    => 'count',
    'hide_empty' => 0,
) );;

/*--------------------------------------------------------------
# Category Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_category_page_panel', array(
    'priority'      => 125,
    'title'         => esc_html__( 'Category Settings', 'sassy' ),
));

foreach ( $category_lists as $single_term ) {
    $term_id     = $single_term->term_id;
    $term_name   = $single_term->name;

    /*--------------------------------------------------------------
    # Each Category Section
    --------------------------------------------------------------*/
    Kirki::add_section( 'sassy_category_'.esc_attr($term_id).'_section', array(
        'title'         => esc_attr($term_name),
        'panel'         => 'sassy_category_page_panel',
        'capability'    => 'edit_theme_options',
    ));

    /*--------------------------------------------------------------
    # Color Control
    --------------------------------------------------------------*/
    Kirki::add_field( 'sassy_config', array(
        'type'        => 'color',
        'settings'    => 'sassy_category_'.esc_attr($term_id).'_header_content_background_color',
        'label'       => esc_attr__( 'Archive Header Background Color', 'sassy' ),
        'section'     => 'sassy_category_'.esc_attr($term_id).'_section',
        'default'   =>  'rgba(255,255,255, 0)',
        'choices'   => array(
            'alpha' => true,
        ),
    ) );

    /*--------------------------------------------------------------
    # Image Control
    --------------------------------------------------------------*/
    Kirki::add_field( 'sassy_config', array(
        'type'        => 'image',
        'settings'    => 'sassy_category_'.esc_attr($term_id).'_header_content_background_image',
        'label'       => esc_attr__( 'Archive Header Background Image', 'sassy' ),
        'description' => esc_html__( 'The recommended size for the background image is 1360x320 pixels', 'sassy' ),
        'section'     => 'sassy_category_'.esc_attr($term_id).'_section',
        'default'     => '',
        'choices'     => array(
            'save_as' => 'id',
        ),
    ) );

    /*--------------------------------------------------------------
    # Radio Control
    --------------------------------------------------------------*/
    Kirki::add_field( 'sassy_config', array(
        'type'        => 'radio',
        'settings'    => 'sassy_category_'.esc_attr($term_id).'_layout',
        'label'       => esc_attr__( 'Select Layout', 'sassy' ),
        'section'     => 'sassy_category_'.esc_attr($term_id).'_section',
        'default'     => 'category-layout-1',
        'choices'     => array(
            'category-layout-1' => esc_attr__( 'First Design', 'sassy' ),
            'category-layout-2' => esc_attr__( 'Second Design', 'sassy' ),
        ),
    ) );
}


