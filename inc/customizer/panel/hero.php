<?php
/**
 * Theme Customizer General Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Panel Hero Section
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_hero_panel', array(
    'priority'  =>  103,
    'title'     =>  esc_html__( 'Hero Section', 'sassy' ),
));

/*--------------------------------------------------------------
# Hero Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_hero_setting_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_hero_panel',
    'capability'    => 'edit_theme_options',
));

/*------------------------------------------------------
# Activate Hero Section Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_section_activate',
    'label'       => esc_html__( 'Homepage', 'sassy' ),
    'description' => esc_html__( 'Enable it to display hero section on Homepage of the site.', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '1',
) );

/*------------------------------------------------------
# Blank Hero Section Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'custom',
    'settings'    => 'sassy_hero_blank_slider_settings',
    'section'     => 'sassy_hero_setting_section',
    'default'     => '<div style="padding: 10px;background-color: #fff; color: #555d66;">' . esc_html__( 'Below settings only work if the content have more then one slide.', 'sassy' ) . '</div>',
) );

/*--------------------------------------------------------------
# Slide Type control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_section_hero_slider_effect',
    'label'       => esc_html__( 'Transition Effect', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => 'slide',
    'choices'     => array(
        'slide'     => esc_html__( 'Slide', 'sassy' ),
        'fade'      => esc_html__( 'Fade', 'sassy' ),
    ),

) );

/*--------------------------------------------------------------
# Slider Speed control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_hero_section_hero_slider_speed',
    'label'       => esc_html__( 'Speed', 'sassy' ),
    'description' => esc_html__( 'Duration of transition between slides (in ms)', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '1200',
    'choices'     => array(
        'min'  => '100',
        'max'  => '2000',
        'step' => '1',
    ),
) );

/*--------------------------------------------------------------
# Slider Delay control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_hero_section_hero_slider_delay',
    'label'       => esc_html__( 'Delay', 'sassy' ),
    'description' => esc_html__( 'Delay between transitions (in ms)', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '3000',
    'choices'     => array(
        'min'  => '0',
        'max'  => '5000',
        'step' => '1',
    ),
) );

/*------------------------------------------------------
# Activate Hero Auto-play Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_section_hero_slider_loop_activate',
    'label'       => esc_html__( 'Loop', 'sassy' ),
    'description' => esc_html__( 'Set enable to continuous loop mode', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '1',
) );

/*------------------------------------------------------
# Activate Hero Navigation Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_section_hero_slider_nav_activate',
    'label'       => esc_html__( 'Arrow Navigation', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '1',
) );

/*------------------------------------------------------
# Activate Hero Pagination Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_section_hero_slider_pagination_activate',
    'label'       => esc_html__( 'Pagination', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => '1',
) );

/*--------------------------------------------------------------
# Pagination Type control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio',
    'settings'    => 'sassy_hero_section_hero_slider_pagination_type',
    'label'       => esc_html__( 'Pagination Type', 'sassy' ),
    'section'     => 'sassy_hero_setting_section',
    'default'     => 'bullets',
    'choices'     => array(
        'bullets'       => esc_html__( 'Bullets', 'sassy' ),
        'fraction'      => esc_html__( 'Fraction', 'sassy' ),
    ),
) );

/*------------------------------------------------------
# Layout Section
-------------------------------------------------------*/
Kirki::add_section( 'sassy_hero_layout_section', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Layout', 'sassy' ),
    'panel'          => 'sassy_hero_panel',
    'capability'     => 'edit_theme_options',
) );

/*------------------------------------------------------
# Hero Header Layout Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_hero_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'section'     => 'sassy_hero_layout_section',
    'default'     => 'hero-layout-1',
    'choices'     => array(
        'hero-layout-1'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-1.svg',
        'hero-layout-2'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-2.svg',
        'hero-layout-3'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-3.svg',
        'hero-layout-4'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-4.svg',
        'hero-layout-5'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-5.svg',
        'hero-layout-6'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-6.svg',
        'hero-layout-7'  => THEME_URI . '/assets/back-end/images/hero/hero-layout-7.svg'
    ),
) );

/*------------------------------------------------------
# Content Section
-------------------------------------------------------*/
Kirki::add_section( 'sassy_hero_content_section', array(
    'priority'       => 4,
    'title'          => esc_html__( 'Content', 'sassy' ),
    'panel'          => 'sassy_hero_panel',
    'capability'     => 'edit_theme_options',
) );

/*------------------------------------------------------
# Hero Slides Control
-------------------------------------------------------*/
Kirki::add_field( 'theme_config_id', array(
    'type'        => 'repeater',
    'section'     => 'sassy_hero_content_section',
    'row_label' => array(
        'type'  => 'field',
        'value' => esc_attr__('Slide', 'sassy' ),
    ),
    'settings'    => 'sassy_repeatable_hero_slides',
    'default'     => array(
        array(
            'hero_slide_image'      => '',
            'hero_slide_title'      => esc_html__( 'Hero Header Title', 'sassy' ),
            'hero_slide_subtitle'   => esc_html__( 'Hero Header Subtitle', 'sassy' ),
            'hero_slide_desc'       => esc_html__( 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat', 'sassy' ),
            'hero_slide_button_text' => esc_html__( 'Discover', 'sassy' ),
            'hero_slide_button_link' => '#',
            'hero_slide_button_link_open' => '_self'
        ),
    ),
    'fields' => array(
        'hero_slide_image' => array(
            'type'        => 'image',
            'label'       => esc_attr__( 'Image', 'sassy' ),
            'default'     => '',
        ),
        'hero_slide_title' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Title', 'sassy' ),
            'default'     => '',
        ),
        'hero_slide_subtitle' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Subtitle', 'sassy' ),
            'default'     => '',
        ),
        'hero_slide_desc' => array(
            'type'        => 'textarea',
            'label'       => esc_attr__( 'Description', 'sassy' ),
            'default'     => '',
        ),
        'hero_slide_button_text' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Button Text', 'sassy' ),
            'default'     => 'Discover',
        ),
        'hero_slide_button_link' => array(
            'type'        => 'link',
            'label'       => esc_attr__( 'Button Link', 'sassy' ),
            'default'     => '#',
        ),
        'hero_slide_button_link_open' => array(
            'type'        => 'radio',
            'label'       => esc_attr__( 'Button Open Link', 'sassy' ),
            'default'     => '_self',
            'choices'     => array(
                '_blank'    => esc_attr__( 'New Tab', 'sassy' ),
                '_self'     => esc_attr__( 'Same Tab', 'sassy' ),
            ),
        ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '!==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*------------------------------------------------------
# Hero Title Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'text',
    'settings'    => 'sassy_hero_title_text',
    'label'       => esc_html__( 'Title', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => esc_html__( 'Hero Header Title', 'sassy' ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h2',
            'function' => 'html',
        ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Title Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'label'       => esc_html__( 'Title Typography', 'sassy' ),
    'settings'    => 'sassy_hero_title_typography',
    'section'     => 'sassy_hero_content_section',
    'default'     => array(
        'font-family'    => 'Libre Baskerville',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
        'color'          => '#181818',
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h2',
        ),
    ),
    'output'        =>  array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h2',
        )
    ),
) );

/*------------------------------------------------------
# Hero SubTitle Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'text',
    'settings'    => 'sassy_hero_subtitle_text',
    'label'       => esc_html__( 'Subtitle', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => esc_html__( 'Hero Header Subtitle', 'sassy' ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h3',
            'function' => 'html',
        ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Subtitle Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'label'       => esc_html__( 'Subtitle Typography', 'sassy' ),
    'settings'    => 'sassy_hero_subtitle_typography',
    'section'     => 'sassy_hero_content_section',
    'default'     => array(
        'font-family'    => 'Libre Baskerville',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
        'color'          => '#727272',
    ),
    'transport' => 'postMessage',
    'js_vars'   => array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h3',
        ),
    ),
    'output'        =>  array(
        array(
            'element'  => '.hero-section .hero-content .entry-title h3',
        )
    ),
) );

/*--------------------------------------------------------------
# Video Type Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_video_type',
    'label'       => esc_html__( 'Video Type', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 'custom',
    'choices'     => array(
        'custom'   => esc_html__( 'Custom', 'sassy' ),
        'youtube'  => esc_html__( 'YouTube', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*------------------------------------------------------
# Custom Video URL Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'upload',
    'settings'    => 'sassy_hero_custom_video_url',
    'label'       => esc_html__( 'Custom Video', 'sassy' ),
    'description' => esc_html__( 'Upload your video in .mp4 format and minimize its file size for best results. Info:- This value is only used for hero layout 7.', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*------------------------------------------------------
# YouTube URL Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'text',
    'settings'    => 'sassy_hero_youtube_video_url',
    'label'       => esc_html__( 'YouTube Video ID', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
        array(
            'setting'  => 'sassy_hero_video_type',
            'operator' => '!==',
            'value'    => 'custom',
        ),
    ),
) );

/*------------------------------------------------------
# Activate Auto Play Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_video_autoplay_enable',
    'label'       => esc_html__( 'AutoPlay', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => '',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Content Text Alignment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_text_alignment',
    'label'       => esc_html__( 'Text Alignment', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 'center',
    'choices'     => array(
        'left'          => esc_attr__( 'Left', 'sassy' ),
        'center'        => esc_attr__( 'Center', 'sassy' ),
        'right'         => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => 'in',
            'value'    => array( 'hero-layout-1', 'hero-layout-2', 'hero-layout-5', 'hero-layout-7' ),
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Content section Border Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_hero_content_border_width',
    'label'       => esc_attr__( 'Border', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '15',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => 'in',
            'value'    => array('hero-layout-2', 'hero-layout-6'),
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Color Scheme Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_content_section_color_scheme',
    'label'       => esc_html__( 'Color Scheme', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 'dark',
    'choices'     => array(
        'dark'   => esc_html__( 'Dark', 'sassy' ),
        'light'  => esc_html__( 'Light', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Background color Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'color',
    'settings'  =>  'sassy_hero_section_background_color',
    'section'   =>  'sassy_hero_content_section',
    'label'     =>  esc_html__( 'Content Background', 'sassy' ),
    'default'   =>  'rgba(255,255,255, 0.25)',
    'choices'   => array(
        'alpha' => true,
    ),
    'transport'     =>  'postMessage',
    'js_vars'       =>  array(
        array(
            'element'   =>  array( '.hero-content' ),
            'function'  =>  'css',
            'property'  =>  'background'
        )
    ),
    'output'        =>  array(
        array(
            'element'   =>  array( '.hero-content' ),
            'function'  =>  'css',
            'property'  =>  'background'
        )
    )
));

/*--------------------------------------------------------------
# Hero Content Section Alignment layout 5 Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_settings_content_section_alignment_layout5',
    'label'       => esc_html__( 'Content Section Alignment', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 'left',
    'choices'     => array(
        'left'          => esc_attr__( 'Left', 'sassy' ),
        'right'         => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback' => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-5',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Content Section Alignment layout 6 Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_settings_content_section_alignment_layout6',
    'label'       => esc_html__( 'Content Section Alignment', 'sassy' ),
    'section'     => 'sassy_hero_content_section',
    'default'     => 'left',
    'choices'     => array(
        'left'          => esc_attr__( 'Left', 'sassy' ),
        'center'        => esc_attr__( 'Center', 'sassy' ),
        'right'         => esc_attr__( 'Right', 'sassy' ),
    ),
    'active_callback' => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-6',
        ),
    ),
) );

/*--------------------------------------------------------------
# Hero Button Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_hero_settings_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'Button', 'sassy' ),
    'panel'         => 'sassy_hero_panel',
    'capability'    => 'edit_theme_options',
));

/*------------------------------------------------------
# Activate Hero Section Control
-------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_settings_button_activate',
    'label'       => esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to display button in hero slide.', 'sassy' ),
    'section'     => 'sassy_hero_settings_section',
    'default'     => '1',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '!==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*--------------------------------------------------------------
# Activate Video icon Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_settings_button_video_icon_activate',
    'label'       => esc_html__( 'Show Video Icon', 'sassy' ),
    'section'     => 'sassy_hero_settings_section',
    'default'     => '1',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '==',
            'value'    => 'hero-layout-7',
        ),
    ),

) );

/*--------------------------------------------------------------
# Button Type Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_hero_settings_button_type',
    'label'       => esc_html__( 'Type', 'sassy' ),
    'section'     => 'sassy_hero_settings_section',
    'default'     => 'arrow',
    'choices'     => array(
        'rounded'       => esc_attr__( 'Rounded', 'sassy' ),
        'arrow'         => esc_attr__( 'Arrow', 'sassy' ),
        'underline'     => esc_attr__( 'Underline', 'sassy' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '!==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );

/*--------------------------------------------------------------
# Button Radius Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_hero_settings_button_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_hero_settings_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_settings_button_type',
            'operator' => '==',
            'value'    => 'rounded',
        ),
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '!==',
            'value'    => 'hero-layout-7',
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-content a.btn.rounded,.hero-video-icon .pt-icon-play::before',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-content a.btn.rounded,.hero-video-icon .pt-icon-play::before',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );

/*--------------------------------------------------------------
# Button Transparency Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_hero_settings_button_transparency',
    'label'       => esc_html__( 'Background Transparent', 'sassy' ),
    'section'     => 'sassy_hero_settings_section',
    'default'     => '1',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_hero_settings_button_type',
            'operator' => 'in',
            'value'    => array( 'rounded' ),
        ),
        array(
            'setting'  => 'sassy_hero_layout',
            'operator' => '!==',
            'value'    => 'hero-layout-7',
        ),
    ),
) );
