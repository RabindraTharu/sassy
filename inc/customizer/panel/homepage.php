<?php
/**
 * Theme Customizer Blog Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Homepage Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_homepage_panel', array(
    'priority'      => 124,
    'title'         => esc_attr__( 'Homepage Settings', 'sassy' ),
));

/*--------------------------------------------------------------
# Front Page Section
--------------------------------------------------------------*/
Kirki::add_section( 'static_front_page', array(
    'priority'      => 1,
    'title'         => esc_attr__( 'Static Front Page', 'sassy' ),
    'panel'         => 'sassy_homepage_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Blog Posts Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_homepage_blog_posts_section', array(
    'priority'      => 3,
    'title'         => esc_attr__( 'Blog Posts', 'sassy' ),
    'panel'         => 'sassy_homepage_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Repeater Post Section Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'repeater',
    'label'       => esc_attr__( 'Homepage Blog Posts Row', 'sassy' ),
    'section'     => 'sassy_homepage_blog_posts_section',
    'settings'    => 'sassy_homepage_repeater_blog_section_layout',
    'default'     => array(
        array(
            'blog_layout_type'  => 'blog-layout-1',
        ),
    ),
    'fields' => array(
        'blog_layout_type' => array(
            'type'        => 'radio-image',
            'label'       => esc_attr__( 'Select Layout Type', 'sassy' ),
            'default'     => 'blog-layout-1',
            'choices'     => array(
                'blog-layout-1'     => THEME_URI . '/assets/back-end/images/blog/blog-row-1.jpg',
                'blog-layout-2'     => THEME_URI . '/assets/back-end/images/blog/blog-row-2.jpg',
                'blog-layout-3'     => THEME_URI . '/assets/back-end/images/blog/blog-row-3.jpg',
                'blog-layout-4'     => THEME_URI . '/assets/back-end/images/blog/blog-row-4.jpg',
                'blog-layout-5'     => THEME_URI . '/assets/back-end/images/blog/blog-row-5.jpg',
            ),
        ),
    )
) );

/*--------------------------------------------------------------
# Date Show Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'toggle',
    'settings'    => 'sassy_homepage_post_date_activate',
    'label'       => esc_attr__( 'Post Date', 'sassy' ),
    'section'     => 'sassy_homepage_blog_posts_section',
    'default'     => 1,
) );
