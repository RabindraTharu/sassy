<?php
/**
 * Theme Customizer Page Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Page Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_page_panel', array(
    'priority'      => 119,
    'title'         => esc_html__( 'Page Settings', 'sassy' ),
));

/*--------------------------------------------------------------
# Breadcrumbs Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_page_breadcrumbs_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Breadcrumbs', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_page_breadcrumbs_activate',
    'section'   =>  'sassy_page_breadcrumbs_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to show breadcrumbs in all pages.', 'sassy' ),
));

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'text',
    'settings'  =>  'sassy_page_breadcrumbs_delimiter',
    'section'   =>  'sassy_page_breadcrumbs_section',
    'label'     =>  esc_html__( 'Delimiter', 'sassy' ),
    'default'   => '/',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_page_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_page_breadcrumbs_margin_y',
    'label'       => esc_attr__( 'Margin - Y', 'sassy' ),
    'section'     => 'sassy_page_breadcrumbs_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '15',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_page_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_page_breadcrumbs_spacing',
    'label'       => esc_attr__( 'Spacing', 'sassy' ),
    'section'     => 'sassy_page_breadcrumbs_section',
    'default'     => 0,
    'choices'     => array(
        'min'  => '0',
        'max'  => '15',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_page_breadcrumbs_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
    'js_vars' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.hero-layout-2 .hero-content,.hero-layout-6 .hero-content',
            'property' => 'border',
            'units'    => 'px solid',
        ),
    ),
) );

/*--------------------------------------------------------------
# Page Header Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_page_header_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Page Header', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));
/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_page_header_activate',
    'section'   =>  'sassy_page_header_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to disable breadcrumbs in all pages.', 'sassy' ),
));

/*--------------------------------------------------------------
# Layout Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_page_layout_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Layout', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Single Page Layout Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_page_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'description' => esc_html__( 'Choose your single page layout.', 'sassy' ),
    'section'     => 'sassy_page_layout_section',
    'default'     => 'page-layout-1',
    'choices'     => array(
        'page-layout-1'         => THEME_URI . '/assets/back-end/images/page/page-layout-1.svg',
    ),
) );

/*--------------------------------------------------------------
# Settings Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_page_settings_section', array(
    'priority'      => 4,
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Content Ordering Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'sortable',
    'settings'    => 'sassy_page_setting_content_order_list',
    'label'       => esc_html__( 'Content Order', 'sassy' ),
    'description' => esc_html__( 'Drag & Drop items to re-arrange order of appearance. Note:- Please enable social share for page to work it through Appearance->Customize->Social->Social Share.', 'sassy' ),
    'section'     => 'sassy_page_settings_section',
    'default'     => array(
        'page-featured-image',
        'page-title',
        'page-content',
    ),
    'choices'     => array(
        'page-featured-image'   => esc_attr__( 'Featured Image', 'sassy' ),
        'page-title'            => esc_attr__( 'Title', 'sassy' ),
        'page-content'          => esc_attr__( 'Content', 'sassy' ),
        'page-social-share'     => esc_attr__( 'Social Share', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Text alignment Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_page_setting_content_text_alignment',
    'label'       => esc_html__( 'Content Alignment', 'sassy' ),
    'section'     => 'sassy_page_settings_section',
    'default'     => 'align-left',
    'choices'     => array(
        'align-left'     => esc_attr__( 'Left', 'sassy' ),
        'align-center'   => esc_attr__( 'Center', 'sassy' ),
        'align-right'    => esc_attr__( 'Right', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_page_sidebar_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'Sidebar', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Global Page Sidebar Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_page_global_sidebar',
    'label'       => esc_html__( 'Page Sidebar', 'sassy' ),
    'description' => esc_html__( 'Select default layout for single page. This layout will be reflected in all single page unless unique layout is set for specific page.', 'sassy' ),
    'section'     => 'sassy_page_sidebar_section',
    'default'     => 'right-sidebar',
    'choices'     => array(
        'left-sidebar'      => THEME_URI . '/assets/back-end/images/sidebar/left-sidebar.svg',
        'full-width'        => THEME_URI . '/assets/back-end/images/sidebar/no-sidebar.svg',
        'right-sidebar'     => THEME_URI . '/assets/back-end/images/sidebar/right-sidebar.svg',

    ),
) );

/*--------------------------------------------------------------
# Page Widgets area Section
--------------------------------------------------------------*/
Kirki::add_section('sassy_page_widgets_section', array(
    'priority'      =>  6,
    'title'         => esc_html__( 'Widgets', 'sassy' ),
    'panel'         => 'sassy_page_panel',
    'capability'    => 'edit_theme_options',
));
