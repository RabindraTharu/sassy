<?php
/**
 * Theme Customizer Social Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Social Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_social_panel', array(
    'priority'      => 102,
    'title'         => esc_html__( 'Social', 'sassy' ),
));

/*--------------------------------------------------------------
# Social Profile Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_social_profile_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Social Profiles', 'sassy' ),
    'panel'         => 'sassy_social_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Repeatable Social Profile Setting & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'                  => 'repeater',
    'label'                 => esc_html__( 'Add Social Profile', 'sassy' ),
    'description'           => esc_html__( 'Drag & Drop items to re-arrange order of appearance.', 'sassy' ),
    'section'               => 'sassy_social_profile_section',
    'row_label'             => array(
        'type'              => 'field',
        'value'             => esc_html__('Social', 'sassy' ),
        'field'             => 'social_name',
    ),
    'settings'              => 'sassy_social_repeatable_social_profiles',
    'default'               => array(
        array(
            'social_name'   => esc_html__( 'Facebook', 'sassy' ),
            'social_url'    => 'https://facebook.com/',
            'social_icon'   => 'fa-facebook',
            'social_image'  => '',

        ),
    ),
    'fields'                => array(
        'social_name'       => array(
            'type'          => 'text',
            'label'         => esc_html__( 'Profile Name', 'sassy' ),
            'default'       => '',
        ),
        'social_url'        => array(
            'type'          => 'text',
            'label'         => esc_html__( 'URL', 'sassy' ),
            'default'       => '',
        ),
        'social_icon'       => array(
            'label'         => esc_html__( 'Icon', 'sassy' ),
            'type'          => 'select',
            'default'       => 'fa-facebook',
            'choices'       => array(
                'fa-behance'        => esc_html__( 'Behance', 'sassy' ),
                'fa-delicious'      => esc_html__( 'Delicious', 'sassy' ),
                'fa-digg'           => esc_html__( 'Digg', 'sassy' ),
                'fa-dribbble'       => esc_html__( 'Dribbble', 'sassy' ),
                'fa-facebook'       => esc_html__( 'Facebook', 'sassy' ),
                'fa-flickr'         => esc_html__( 'Flickr', 'sassy' ),
                'fa-foursquare'     => esc_html__( 'Foursquare', 'sassy' ),
                'fa-github'         => esc_html__( 'Github', 'sassy' ),
                'fa-google-plus'     => esc_html__( 'Google Plus', 'sassy' ),
                'fa-instagram'      => esc_html__( 'Instagram', 'sassy' ),
                'fa-linkedin'       => esc_html__( 'LinkedIn', 'sassy' ),
                'fa-envelope'       => esc_html__( 'Mail', 'sassy' ),
                'fa-medium'         => esc_html__( 'Medium', 'sassy' ),
                'fa-pinterest'      => esc_html__( 'Pinterest', 'sassy' ),
                'fa-reddit'         => esc_html__( 'Reddit', 'sassy' ),
                'fa-skype'          => esc_html__( 'Skype', 'sassy' ),
                'fa-slack'          => esc_html__( 'Slack', 'sassy' ),
                'fa-stackoverflow'  => esc_html__( 'Stackoverflow', 'sassy' ),
                'fa-twitter'        => esc_html__( 'Twitter', 'sassy' ),
                'fa-tumblr'         => esc_html__( 'Tumblr', 'sassy' ),
                'fa-vimeo'          => esc_html__( 'Vimeo', 'sassy' ),
                'fa-youtube'        => esc_html__( 'Youtube', 'sassy' ),
            )
        ),
        'social_image'      => array(
            'type'          => 'image',
            'label'         => esc_html__( 'Custom Icon', 'sassy' ),
            'default'       => '',
        ),
    ),
) );

/*--------------------------------------------------------------
# Social Profile border Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_social_profile_button_border_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_social_profile_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '30',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'js_vars' => array(
        array(
            'element'  => '.social-profiles-widget ul li',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.social-profiles-widget ul li',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );

/*--------------------------------------------------------------
# Social Share Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_social_share_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Social Share', 'sassy' ),
    'panel'         => 'sassy_social_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Social Share Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'text',
    'settings'      =>  'sassy_social_share_block_title_text',
    'label'         =>  esc_html__( 'Share Text', 'sassy' ),
    'section'       =>  'sassy_social_share_section',
    'default'       =>  esc_html__( 'Share This On: ', 'sassy' ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'  => '.content-holder .social-share-wrap label',
            'function' => 'html',
        ),
    ),
));

/*--------------------------------------------------------------
# Social Share Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'sortable',
    'settings'    => 'sassy_social_share_sortable_lists',
    'label'       => esc_html__( 'Available Social Share', 'sassy' ),
    'description' => esc_html__( 'Drag & Drop items to re-arrange order of appearance.', 'sassy' ),
    'section'     => 'sassy_social_share_section',
    'default'     => array(
        'facebook',
        'twitter',
        'google'
    ),
    'choices'     => array(
        'facebook'      => esc_attr__( 'Facebook', 'sassy' ),
        'twitter'       => esc_attr__( 'Twitter', 'sassy' ),
        'google'        => esc_attr__( 'Google+', 'sassy' ),
        'tumblr'        => esc_attr__( 'Tumblr', 'sassy' ),
        'linkedin'      => esc_attr__( 'Linkedin', 'sassy' ),
        'reddit'        => esc_attr__( 'Reddit', 'sassy' ),
        'pinterest'     => esc_attr__( 'Pinterest', 'sassy' ),
        'email'         => esc_attr__( 'Email', 'sassy' ),
    ),
) );

/*--------------------------------------------------------------
# Button border Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'slider',
    'settings'    => 'sassy_social_share_button_border_radius',
    'label'       => esc_attr__( 'Border Radius', 'sassy' ),
    'section'     => 'sassy_social_share_section',
    'default'     => 3,
    'choices'     => array(
        'min'  => '0',
        'max'  => '30',
        'step' => '1',
    ),
    'transport'        =>  'postMessage',
    'js_vars' => array(
        array(
            'element'  => '.social-share-wrap li a',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
    'output' => array(
        array(
            'element'  => '.social-share-wrap li a',
            'property' => 'border-radius',
            'units'    => 'px',
        ),
    ),
) );
