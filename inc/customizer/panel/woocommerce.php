<?php
/**
 * Envy Blog Customizer WooCommerce Panel
 *
 * @package Envy Blog Pro
 */

if( !class_exists( 'WooCommerce' ) )
    return;

if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.3', '<' ) ) {
    /*--------------------------------------------------------------
    # WooCommerce Panel
    --------------------------------------------------------------*/
    Kirki::add_panel( 'woocommerce', array(
        'priority'      => 121,
        'title'         => esc_html__( 'WooCommerce', 'sassy' ),
    ));
}

/*--------------------------------------------------------------
# WooCommerce Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_wc_product_page_sidebar_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Sidebar', 'sassy' ),
    'panel'         => 'woocommerce',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# WooCommerce Product Page Sidebar Layout
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_wc_product_page_global_sidebar',
    'label'       => esc_html__( 'Product Sidebar', 'sassy' ),
    'description' => esc_html__( 'Select default sidebar layout. This layout will be reflected on product page of WooCommerce.', 'sassy' ),
    'section'     => 'sassy_wc_product_page_sidebar_section',
    'default'     => 'full-width',
    'choices'     => array(
        'left-sidebar'      => ENVY_BLOG_URI . '/inc/assets/images/sidebar/left-sidebar.svg',
        'full-width'        => ENVY_BLOG_URI . '/inc/assets/images/sidebar/no-sidebar.svg',
        'right-sidebar'     => ENVY_BLOG_URI . '/inc/assets/images/sidebar/right-sidebar.svg',

    ),
) );

/*--------------------------------------------------------------
# WooCommerce Product Page Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_wc_related_upsells_crosssells_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Related,Upsells,Crosssells Settings', 'sassy' ),
    'panel'         => 'woocommerce',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# WooCommerce Setting Related Product Per Row
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_wc_product_page_related_product_per_row',
    'label'       => esc_html__( 'Related Product Per Row', 'sassy' ),
    'description' => esc_html__( 'Choose the number of products in a row for related products shown in single product page.', 'sassy' ),
    'section'     => 'sassy_wc_related_upsells_crosssells_section',
    'default'     => '4',
    'choices'     => array(
        '2'     => '2',
        '3'     => '3',
        '4'     => '4',
        '5'     => '5',
    ),
) );

/*--------------------------------------------------------------
# WooCommerce Setting Upsell Product Per Row
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_wc_product_page_upsell_product_per_row',
    'label'       => esc_html__( 'Up-Sells Products Per Row', 'sassy' ),
    'description' => esc_html__( 'Choose the number of products in a row for upsell products shown in single product page.', 'sassy' ),
    'section'     => 'sassy_wc_related_upsells_crosssells_section',
    'default'     => 4,
    'choices'     => array(
        '2'     => '2',
        '3'     => '3',
        '4'     => '4',
        '5'     => '5',
    ),
) );

/*--------------------------------------------------------------
# WooCommerce Setting Cross Sell Product Per Row
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'select',
    'settings'    => 'sassy_wc_cart_page_crosssell_product_per_row',
    'label'       => esc_html__( 'Cross-Sells Products Per Row', 'sassy' ),
    'description' => esc_html__( 'Choose the number of products in a row for cross-sells products shown in cart page.', 'sassy' ),
    'section'     => 'sassy_wc_related_upsells_crosssells_section',
    'default'     => 4,
    'choices'     => array(
        '2'     => '2',
        '3'     => '3',
        '4'     => '4',
        '5'     => '5',
    ),
) );
