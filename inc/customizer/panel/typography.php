<?php
/**
 * Envy Blog Customizer General Panel
 *
 * @package Envy Blog Pro
 */

/*--------------------------------------------------------------
# Typography Panel
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_typography_panel', array(
    'priority'      => 101,
    'title'         => esc_html__( 'Typography', 'sassy' ),
));

/*--------------------------------------------------------------
# Body Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_body_section', array(
    'priority'      => 1,
    'title'         => esc_html__( 'Body', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Body Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_body',
    'label'       => esc_attr__( 'Body', 'sassy' ),
    'section'     => 'sassy_typography_body_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'body',
        ),
    ),
) );

/*--------------------------------------------------------------
# Logo Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_logo_section', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Logo', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Site Title Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_site_title',
    'label'       => esc_attr__( 'Site Title', 'sassy' ),
    'section'     => 'sassy_typography_logo_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => '.site-branding .site-title',
        ),
    ),
) );

/*--------------------------------------------------------------
# Tagline Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_site_tagline',
    'label'       => esc_attr__( 'Site Tagline', 'sassy' ),
    'section'     => 'sassy_typography_logo_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => '.site-branding p',
        ),
    ),
) );

/*--------------------------------------------------------------
# Main Menu Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_main_menu_section', array(
    'priority'      => 3,
    'title'         => esc_html__( 'Main Menu', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Main Menu Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_main_menu',
    'label'       => esc_attr__( 'Main Menu', 'sassy' ),
    'section'     => 'sassy_typography_main_menu_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => '.main-navigation ul li',
        ),
    ),
) );

/*--------------------------------------------------------------
# H1 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h1_section', array(
    'priority'      => 4,
    'title'         => esc_html__( 'H1', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H1 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h1',
    'label'       => esc_attr__( 'H1', 'sassy' ),
    'section'     => 'sassy_typography_heading_h1_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h1',
        ),
    ),
) );

/*--------------------------------------------------------------
# H2 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h2_section', array(
    'priority'      => 5,
    'title'         => esc_html__( 'H2', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H2 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h2',
    'label'       => esc_attr__( 'H2', 'sassy' ),
    'section'     => 'sassy_typography_heading_h2_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => '500',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h2',
        ),
    ),
) );

/*--------------------------------------------------------------
# H3 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h3_section', array(
    'priority'      => 6,
    'title'         => esc_html__( 'H3', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H3 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h3',
    'label'       => esc_attr__( 'H3', 'sassy' ),
    'section'     => 'sassy_typography_heading_h3_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h3',
        ),
    ),
) );

/*--------------------------------------------------------------
# H4 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h4_section', array(
    'priority'      => 7,
    'title'         => esc_html__( 'H4', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H4 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h4',
    'label'       => esc_attr__( 'H4', 'sassy' ),
    'section'     => 'sassy_typography_heading_h4_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h4',
        ),
    ),
) );

/*--------------------------------------------------------------
# H5 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h5_section', array(
    'priority'      => 8,
    'title'         => esc_html__( 'H5', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H5 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h5',
    'label'       => esc_attr__( 'H5', 'sassy' ),
    'section'     => 'sassy_typography_heading_h5_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h5',
        ),
    ),
) );

/*--------------------------------------------------------------
# H6 Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_heading_h6_section', array(
    'priority'      => 9,
    'title'         => esc_html__( 'H6', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# H6 Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_heading_h6',
    'label'       => esc_attr__( 'H6', 'sassy' ),
    'section'     => 'sassy_typography_heading_h6_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => 'h6',
        ),
    ),
) );

/*--------------------------------------------------------------
# Post Meta Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_post_meta_section', array(
    'priority'      => 10,
    'title'         => esc_html__( 'Post Meta', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Post Meta Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_post_meta',
    'label'       => esc_attr__( 'Post Meta', 'sassy' ),
    'section'     => 'sassy_typography_post_meta_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => '.entry-meta label',
        ),
    ),
) );

/*--------------------------------------------------------------
# Widget Title Typography Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_typography_widgets_title_section', array(
    'priority'      => 11,
    'title'         => esc_html__( 'Widget Title', 'sassy' ),
    'panel'         => 'sassy_typography_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Widget Title Typography & Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_typography_widgets_title',
    'label'       => esc_attr__( 'Widget Title', 'sassy' ),
    'section'     => 'sassy_typography_widgets_title_section',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'subsets'        => array( 'latin-ext' ),
    ),
    'output'      => array(
        array(
            'element' => '.widget-title',
        ),
    ),
) );
