<?php
/**
 * Theme Customizer General Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Panel General
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_general_panel', array(
    'priority'  =>  100,
    'title'     =>  esc_html__( 'General', 'sassy' ),
));
/*--------------------------------------------------------------
# Smooth Scroll Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_general_smooth_scroll_section', array(
    'priority'      =>  4,
    'title'         => esc_html__( 'Smooth Scroll', 'sassy' ),
    'panel'         => 'sassy_general_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Smooth Scroll Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_general_smooth_scroll_activate',
    'section'   =>  'sassy_general_smooth_scroll_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to set smooth scroll in site.', 'sassy' ),
));

/*--------------------------------------------------------------
# Smooth Scroll Timing Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'number',
    'settings'  =>  'sassy_general_smooth_scroll_timing',
    'section'   =>  'sassy_general_smooth_scroll_section',
    'label'     =>  esc_html__( 'Scroll Timing', 'sassy' ),
    'active_callback'  => array(
        array(
            'setting'  => 'sassy_general_smooth_scroll_activate',
            'operator' => '==',
            'value'    => true,
        ),
    ),
));

/*--------------------------------------------------------------
# Human Time Diff Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_general_human_time_diff_section', array(
    'priority'      =>  5,
    'title'         => esc_html__( 'Human Time Diff', 'sassy' ),
    'panel'         => 'sassy_general_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Human Time Diff Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_general_human_time_diff_activate',
    'section'   =>  'sassy_general_human_time_diff_section',
    'label'     =>  esc_html__( 'Activate', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to set posted time as humna time difference.', 'sassy' ),
    'default'   =>  '1'
));

/*--------------------------------------------------------------
# Sidebar Section
--------------------------------------------------------------*/
Kirki::add_section( 'envy-sassy_general_sidebar_section', array(
    'priority'      =>  6,
    'title'         => esc_html__( 'Sidebar Settings', 'sassy' ),
    'panel'         => 'sassy_general_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Activate Sticky Sidebar Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'      =>  'toggle',
    'settings'  =>  'sassy_general_sticky_sidebar_activate',
    'section'   =>  'envy-sassy_general_sidebar_section',
    'label'     =>  esc_html__( 'Sticky Sidebar', 'sassy' ),
    'description' =>  esc_html__( 'Enable it to make sidebar area sticky.', 'sassy' ),
    'default'   =>  '1'
));

/*--------------------------------------------------------------
# Activate Sidebar Separator Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_general_sidebar_seperator_activate',
    'section'       =>  'envy-sassy_general_sidebar_section',
    'label'         =>  esc_html__( 'Sidebar Separator', 'sassy' ),
    'description'   =>  esc_html__( 'Enable it to display sidebar separator line in between main content & sidebar content section.', 'sassy' ),
    'default'       =>  '1',
));
