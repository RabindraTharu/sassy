<?php
/**
 * Theme Customizer Header Panel
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Panel Header
--------------------------------------------------------------*/
Kirki::add_panel( 'sassy_header_panel', array(
    'priority'      => 2,
    'title'         => esc_html__( 'Header', 'sassy' ),
));

/*--------------------------------------------------------------
# Site Title & Tag-Line Section
--------------------------------------------------------------*/
Kirki::add_section( 'title_tagline', array(
    'title'         => esc_html__( 'Site Title & Tagline', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'priority'      => 8,
    'type'          => 'slider',
    'settings'      => 'sassy_site_branding_logo',
    'label'         => esc_html__( 'Logo Height', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '72',
    'choices'       => array(
        'min'  => '0',
        'max'  => '200',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .custom-logo' ),
            'property'      => 'height',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .custom-logo' ),
            'property'      => 'height',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Text Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'priority'      => 9,
    'type'          => 'text',
    'settings'      => 'sassy_header_short_title',
    'section'       => 'title_tagline',
    'label'         => esc_html__( 'Short Site Title', 'sassy' ),
    'description'   => esc_html__( 'Setting this will replace Site Title while displaying site header.', 'sassy' ),
    'default'       => esc_attr__( '', 'textdomain' ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'toggle',
    'settings'      => 'sassy_header_site_title_visible',
    'section'       => 'title_tagline',
    'label'         => esc_html__( 'Display Site Title', 'sassy' ),
    'default'       => '1',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'              => 'toggle',
    'settings'          => 'sassy_header_site_tagline_visible',
    'section'           => 'title_tagline',
    'label'             => esc_html__( 'Display Tagline', 'sassy' ),
    'default'           => '1',
));

/*--------------------------------------------------------------
# Typography Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_header_site_title_typography',
    'label'       => esc_attr__( 'Site Title Typography', 'sassy' ),
    'section'     => 'title_tagline',
    'default'     => array(
        'font-family'    => 'Montserrat',
        'variant'        => '500',
        'font-size'      => '32px',
        'line-height'    => '1.2',
        'letter-spacing' => '0',
        'color'          => '#000',
        'text-transform' => 'none',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'   => array( '.site-branding .site-title a' ),
        )
    ),
    'output'        =>  array(
        array(
            'element'   => array( '.site-branding .site-title a' ),
        )
    ),
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_site_title_border',
    'label'         => esc_html__( 'Site Title Border', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '2',
    'choices'       => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'border-width',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'border-width',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_site_title_padding_top',
    'label'         => esc_html__( 'Site Title Padding Top', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '12',
    'choices'       => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_site_title_padding_right',
    'label'         => esc_html__( 'Site Title Padding Right', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '10',
    'choices'       => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-right',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-right',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_site_title_padding_bottom',
    'label'         => esc_html__( 'Site Title Padding Bottom', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '2',
    'choices'       => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_site_title_padding_left',
    'label'         => esc_html__( 'Site Title Padding Left', 'sassy' ),
    'section'       => 'title_tagline',
    'default'       => '10',
    'choices'       => array(
        'min'  => '0',
        'max'  => '50',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-left',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-branding .site-title a' ),
            'property'      => 'padding-left',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Typography Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'typography',
    'settings'    => 'sassy_header_site_description_typography',
    'label'       => esc_attr__( 'Site Description Typography', 'sassy' ),
    'section'     => 'title_tagline',
    'default'     => array(
        'font-family'    => 'Roboto',
        'variant'        => 'regular',
        'font-size'      => '12px',
        'line-height'    => '1.2',
        'letter-spacing' => '0.5',
        'color'          => '#b2b2b2',
        'text-transform' => 'none',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'   => array( '.site-branding .site-description' ),
        )
    ),
    'output'        =>  array(
        array(
            'element'   => array( '.site-branding .site-description' ),
        )
    ),
) );

/*--------------------------------------------------------------
# Header Language Switcher Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_header_language_switcher_section', array(
    'title'         => esc_html__( 'Language Switcher', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'toggle',
    'settings'      => 'sassy_header_language_switcher_enable',
    'section'       => 'sassy_header_language_switcher_section',
    'label'         => esc_html__( 'Enable', 'sassy' ),
    'description'   => esc_html__( 'This theme is fully WPML compatible for multilingual. Get WPML plugin here(link) or Contact(link) us for best offer! Customize more option via plugin page. ', 'sassy' ),    'default'       => '1',
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'select',
    'settings'      => 'sassy_header_language_switcher_display_type',
    'section'       => 'sassy_header_language_switcher_section',
    'label'         => esc_html__( 'Show', 'sassy' ),
    'default'       => 'in-line',
    'choices'       => array(
        'in-line'      => esc_html__( 'List', 'sassy' ),
        'drop-down'    => esc_html__( 'Drop Down', 'sassy' ),
    ),
));

/*--------------------------------------------------------------
# Header Search Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_header_search_section', array(
    'title'         => esc_html__( 'Header Search', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'toggle',
    'settings'      => 'sassy_header_search_enable',
    'section'       => 'sassy_header_search_section',
    'label'         => esc_html__( 'Enable', 'sassy' ),
    'default'       => 1,
));

/*--------------------------------------------------------------
# Radio Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'radio-image',
    'settings'      => 'sassy_header_search_column',
    'label'         => esc_html__( 'Number of Columns', 'sassy' ),
    'description'   => esc_html__( 'Each column is widgets position where you can add widgets.', 'sassy' ),
    'section'       => 'sassy_header_search_section',
    'default'       => 'header-search-columns-1',
    'choices'       => array(
        'header-search-columns-1'         => THEME_URI . '/assets/back-end/images/header/layout-1-column.svg',
        'header-search-columns-2'         => THEME_URI . '/assets/back-end/images/header/layout-2-column.svg',
        'header-search-columns-3'         => THEME_URI . '/assets/back-end/images/header/layout-3-column.svg',
    ),
) );

/*--------------------------------------------------------------
# Slide Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_header_search_bg_opacity',
    'label'         => esc_html__( 'Background Opacity', 'sassy' ),
    'section'       => 'sassy_header_search_section',
    'default'       => '1',
    'choices'       => array(
        'min'  => '0',
        'max'  => '1',
        'step' => '.1',
    ),
    'transport'     => 'postMessage',
) );

/*--------------------------------------------------------------
# Header Slide-in Box Section
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_header_slide_in_box_section', array(
    'title'         => esc_html__( 'Slide-in Box', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'toggle',
    'settings'      => 'sassy_header_slide_in_box_activate',
    'section'       => 'sassy_header_slide_in_box_section',
    'label'         => esc_html__( 'Enable', 'sassy' ),
    'description'   => esc_html__( 'Slides-in Widgets Position Sidebar triggered with 3 dots icon.', 'sassy' ),
    'default'       => '1',
));

/*--------------------------------------------------------------
# Select Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'select',
    'settings'      => 'sassy_header_slide_in_box_position',
    'section'       => 'sassy_header_slide_in_box_section',
    'label'         => esc_html__( 'Slide-in Position', 'sassy' ),
    'default'       => 'from-right',
    'choices'       => array(
        'from-top'      => esc_html__( 'From Top', 'sassy' ),
        'from-right'    => esc_html__( 'From Right', 'sassy' ),
        'from-bottom'   => esc_html__( 'From Bottom', 'sassy' ),
        'from-left'     => esc_html__( 'From Left', 'sassy' ),
    ),
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'toggle',
    'settings'      => 'sassy_header_slide_in_box_fade_in_activate',
    'section'       => 'sassy_header_slide_in_box_section',
    'label'         => esc_html__( 'Fade-in Effects', 'sassy' ),
    'description'   => esc_html__( 'Slides-in Box appears with fade-in effects.', 'sassy' ),
    'default'       => '1',
    'transport'     => 'postMessage',
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_header_slide_in_box_width',
    'label'         => esc_html__( 'Width', 'sassy' ),
    'section'       => 'sassy_header_slide_in_box_section',
    'default'       => '400',
    'choices'       => array(
        'min'  => '0',
        'max'  => '750',
        'step' => '10',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.---- .----' ),
            'property'      => 'width',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.--------' ),
            'property'      => 'width',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_header_slide_in_box_padding',
    'section'       => 'sassy_header_slide_in_box_section',
    'label'         => esc_html__( 'Padding', 'sassy' ),
    'default'       => '100',
    'choices'       => array(
        'min'  => '0',
        'max'  => '300',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-right',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-left',
            'units'         => 'px',
        ),
    ),
    'output'        => array(
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-right',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.--------' ),
            'property'      => 'padding-left',
            'units'         => 'px',
        ),
    )
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_header_slide_in_box_bg_opacity',
    'label'         => esc_html__( 'Background Opacity', 'sassy' ),
    'section'       => 'sassy_header_slide_in_box_section',
    'default'       => '1',
    'choices'       => array(
        'min'  => '0',
        'max'  => '1',
        'step' => '.1',
    ),
    'transport'     => 'postMessage',
) );

/*--------------------------------------------------------------
# Header Layouts
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_header_layout_section', array(
    'title'         => esc_html__( 'Layout', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Radio Image Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'        => 'radio-image',
    'settings'    => 'sassy_header_layout',
    'label'       => esc_html__( 'Layout', 'sassy' ),
    'section'     => 'sassy_header_layout_section',
    'default'     => 'header-layout-1',
    'choices'     => array(
        'header-layout-1'         => THEME_URI . '/assets/back-end/images/header/layout-1.svg',
        'header-layout-2'         => THEME_URI . '/assets/back-end/images/header/layout-2.svg',
        'header-layout-3'         => THEME_URI . '/assets/back-end/images/header/layout-3.svg',
        'header-layout-4'         => THEME_URI . '/assets/back-end/images/header/layout-4.svg',
        'header-layout-5'         => THEME_URI . '/assets/back-end/images/header/layout-5.svg',
        'header-layout-6'         => THEME_URI . '/assets/back-end/images/header/layout-6.svg',
    ),
) );

/*--------------------------------------------------------------
# Header Settings
--------------------------------------------------------------*/
Kirki::add_section( 'sassy_sticky_header_section', array(
    'title'         => esc_html__( 'Settings', 'sassy' ),
    'panel'         => 'sassy_header_panel',
    'capability'    => 'edit_theme_options',
));

/*--------------------------------------------------------------
# Toggle Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          =>  'toggle',
    'settings'      =>  'sassy_sticky_header_activate',
    'section'       =>  'sassy_sticky_header_section',
    'label'         =>  esc_html__( 'Sticky Header', 'sassy' ),
    'description'   =>  esc_html__( 'Header Bar stick always on top., ', 'sassy' ),
));

/*--------------------------------------------------------------
# Slider Control
--------------------------------------------------------------*/
Kirki::add_field( 'sassy_config', array(
    'type'          => 'slider',
    'settings'      => 'sassy_header_padding_y',
    'label'         => esc_html__( 'Header Padding (Top & Bottom)', 'sassy' ),
    'section'       => 'sassy_sticky_header_section',
    'default'       => '40',
    'choices'       => array(
        'min'  => '0',
        'max'  => '100',
        'step' => '1',
    ),
    'transport'     => 'postMessage',
    'js_vars'       => array(
        array(
            'element'       => array( '.site-header' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.site-header' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        )
    ),
    'output'        => array(
        array(
            'element'       => array( '.site-header' ),
            'property'      => 'padding-top',
            'units'         => 'px',
        ),
        array(
            'element'       => array( '.site-header' ),
            'property'      => 'padding-bottom',
            'units'         => 'px',
        )
    )
) );

/*--------------------------------------------------------------
# Site Header Separator
--------------------------------------------------------------*/
//Kirki::add_field( 'sassy_config', array(
//    'type'      =>  'toggle',
//    'settings'  =>  'sassy_sticky_header_separator',
//    'section'   =>  'sassy_sticky_header_section',
//    'label'     =>  esc_html__( 'Header Separator', 'sassy' ),
//    'transport'     => 'postMessage',
//    'js_vars'       => array(
//        array(
//            'element'       => array( '.site-header' ),
//            'property'      => 'padding-top',
//            'units'         => 'px',
//        ),
//        array(
//            'element'       => array( '.site-header' ),
//            'property'      => 'padding-bottom',
//            'units'         => 'px',
//        )
//    ),
//    'output'        => array(
//        array(
//            'element'       => array( '.site-header' ),
//            'property'      => 'padding-top',
//            'units'         => 'px',
//        ),
//        array(
//            'element'       => array( '.site-header' ),
//            'property'      => 'padding-bottom',
//            'units'         => 'px',
//        )
//    )
//));









