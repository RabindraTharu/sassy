<?php
/**
 * @package Envy Blog Pro
 */

add_action( 'wp_head', 'sassy_custom_style' );
/**
 * Hooks the Custom Internal CSS to head section
 */
function sassy_custom_style() {

	$container_background_color = get_theme_mod( 'sassy_general_container_background_color', '#ffffff' );

	$custom = '';

	if( !empty( $custom ) ) {
		echo '<!-- '.get_bloginfo('name').' Internal Styles -->';
	?>
		<style type="text/css"><?php echo $custom; ?></style>
	<?php
	}
}