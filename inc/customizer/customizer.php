<?php
/**
 * Theme Customizer
 *
 * @package Sassy
 */

/*--------------------------------------------------------------
# Configuration for Kirki Toolkit
--------------------------------------------------------------*/
function sassy_kirki_configuration() {
    return array( 'url_path'     => THEME_URI . '/inc/libraries/kirki/' );
}
add_filter( 'kirki/config', 'sassy_kirki_configuration' );

/*--------------------------------------------------------------
# sassy Kirki Config
--------------------------------------------------------------*/
Kirki::add_config( 'sassy_config', array(
    'capability'    => 'edit_theme_options',
    'option_type'   => 'theme_mod',
) );

// Panels
require THEME_DIR . '/inc/customizer/panel/coming-soon.php';
require THEME_DIR . '/inc/customizer/panel/header.php';
require THEME_DIR . '/inc/customizer/panel/general.php';
require THEME_DIR . '/inc/customizer/panel/social.php';
require THEME_DIR . '/inc/customizer/panel/hero.php';
require THEME_DIR . '/inc/customizer/panel/page.php';
require THEME_DIR . '/inc/customizer/panel/homepage.php';
require THEME_DIR . '/inc/customizer/panel/404-page.php';
require THEME_DIR . '/inc/customizer/panel/post.php';
require THEME_DIR . '/inc/customizer/panel/blog.php';
require THEME_DIR . '/inc/customizer/panel/archive-header.php';
require THEME_DIR . '/inc/customizer/panel/footer.php';

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function sassy_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

    // Remove
    $wp_customize->remove_control( 'display_header_text' );
    $wp_customize->remove_control( 'header_textcolor' );
    $wp_customize->remove_section( 'background_image' );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'sassy_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'sassy_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'sassy_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function sassy_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function sassy_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function sassy_customize_preview_js() {
	wp_enqueue_script( 'sassy-customizer-script', THEME_URI . '/assets/back-end/js/customizer-script.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'sassy_customize_preview_js' );
