<?php
/**
 * Custom template Useful functions that return arrays for theme
 *
 * @package Sassy
 */

if ( ! function_exists( 'sassy_content_layouts' ) ) :
    /**
     * Returns array of content layouts for page or page.
     */
    function sassy_content_layouts( $output = array() ) {

        $output['left-sidebar']     = esc_html__( 'Left Sidebar', 'sassy' );
        $output['full-width']       = esc_html__( 'Full Width', 'sassy' );
        $output['right-sidebar']    = esc_html__( 'Right Sidebar', 'sassy' );
        return $output;
    }
endif;

if ( ! function_exists( 'sassy_enable_elements' ) ) :
    /**
     * Returns array of element for page or page.
     */
    function sassy_enable_elements( $output = array() ) {

        $output['enable']   = esc_html__( 'Enable', 'sassy' );
        $output['disable']  = esc_html__( 'Enable', 'sassy' );
        return $output;
    }
endif;

if ( ! function_exists( 'sassy_available_sidebars' ) ) :
    /**
     * Returns array of available sidebars.
     */
    function sassy_available_sidebars( $output = array() ) {
        global $wp_registered_sidebars;

        foreach ( $wp_registered_sidebars as $sidebar ) {
            if ( ! in_array( $sidebar['name'], apply_filters( 'sassy_sidebars_exclude', array( 'Display Everywhere' ) ) ) ) {
                $output[ $sidebar['id'] ] = $sidebar['name'];
            }
        }

        return $output;
    }
endif;

if ( ! function_exists( 'sassy_gallery_image_ratio' ) ) :
    /**
     * Returns array of gallery image ratio for page or page.
     */
    function sassy_gallery_image_ratio( $output = array() ) {

        $output['16x9']   = esc_html__( '16 : 9 Ratio', 'sassy' );
        $output['4x3']    = esc_html__( '4 : 3 Ratio', 'sassy' );
        return $output;
    }
endif;

if ( ! function_exists( 'sassy_col_per_row' ) ) :
    /**
     * Returns array of gallery image ratio for page or page.
     */
    function sassy_col_per_row( $output = array() ) {

        $output['col-2']   = 2;
        $output['col-3']   = 3;
        $output['col-4']   = 4;
        $output['col-5']   = 5;
        return $output;
    }
endif;

if ( ! function_exists( 'sassy_gallery_position' ) ) :
    /**
     * Returns array of gallery image position for page or page.
     */
    function sassy_gallery_position( $output = array() ) {

        $output['before-content']  = esc_html__( 'Before Content', 'sassy' );
        $output['after-content']   = esc_html__( 'After Content', 'sassy' );
        return $output;
    }
endif;
