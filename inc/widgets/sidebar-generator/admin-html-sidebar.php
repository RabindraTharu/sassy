<?php
/**
 * Admin View: Template - Sidebars
 *
 * @package Sassy
 */

/*----------------------------------------------------------------------
# Exit if accessed directly
-------------------------------------------------------------------------*/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$nonce = wp_create_nonce ( 'delete-sassy-widget_area-nonce' );
?>
<script type="text/html" id="sassy-add-widget-template">
    <div id="sassy-add-widget" class="widgets-holder-wrap">
        <input type="hidden" name="sassy-nonce" value="<?php echo esc_attr( $nonce ); ?>" />
        <div class="sidebar-name">
            <h3><?php esc_html_e( 'Create Widget Area', 'sassy' ); ?></h3>
        </div>
        <div class="sidebar-description">
            <form id="addWidgetAreaForm" action="" method="post">
                <div class="widget-content">
                    <input id="sassy-add-widget-input" name="sassy-add-widget-input" type="text" class="regular-text" title="<?php esc_attr_e( 'Name', 'sassy' ); ?>" placeholder="<?php esc_attr_e( 'Name', 'sassy' ); ?>" />
                </div>
                <div class="widget-control-actions">
                    <input class="add-widget-area-button button-primary" type="submit" value="<?php esc_attr_e( 'Save', 'sassy' ); ?>" />
                </div>
            </form>
        </div>
    </div>
</script>
