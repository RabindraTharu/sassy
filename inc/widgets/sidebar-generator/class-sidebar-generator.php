<?php
/**
 * Custom Sidebar Generator
 *
 * @package Sassy
 */

/*----------------------------------------------------------------------
# Exit if accessed directly
-------------------------------------------------------------------------*/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/*----------------------------------------------------------------------
# Start Class Sassy_Custom_Sidebars
-------------------------------------------------------------------------*/
if ( ! class_exists( 'Sassy_Custom_Sidebars' ) ) {

    class Sassy_Custom_Sidebars {

        protected $widget_areas	= array();
        protected $default_area = array();

        /**
         * Start things up
         */
        public function __construct() {

            add_action( 'init', array( $this, 'register_sidebars' ) , 1000 );
            add_action( 'admin_print_scripts-widgets.php', array( $this, 'add_widget_box' ) );
            add_action( 'load-widgets.php', array( $this, 'add_widget_area' ), 100 );
            add_action( 'load-widgets.php', array( $this, 'scripts' ), 100 );
            add_action( 'wp_ajax_sassy_delete_widget_area', array( $this, 'sassy_delete_widget_area' ) );
        }

        /**
         * Add the widget box inside a script
         */
        public function add_widget_box() {
            require THEME_DIR . '/inc/widgets/sidebar-generator/admin-html-sidebar.php';
        }

        /**
         * Create new Widget Area
         */
        public function add_widget_area() {
            if ( ! empty( $_POST['sassy-add-widget-input'] ) ) {
                $this->widget_areas = $this->get_widget_areas();
                array_push( $this->widget_areas, $this->check_widget_area_name( $_POST['sassy-add-widget-input'] ) );
                $this->save_widget_areas();
                wp_redirect( admin_url( 'widgets.php' ) );
                die();
            }
        }

        /**
         * Before we create a new widget_area, verify it doesn't already exist. If it does, append a number to the name.
         */
        public function check_widget_area_name( $name ) {
            if ( empty( $GLOBALS['wp_registered_widget_areas'] ) ) {
                return $name;
            }

            $taken = array();
            foreach ( $GLOBALS['wp_registered_widget_areas'] as $widget_area ) {
                $taken[] = $widget_area['name'];
            }

            $taken = array_merge( $taken, $this->widget_areas );

            if ( in_array( $name, $taken ) ) {
                $counter  = substr( $name, -1 );
                $new_name = "";

                if ( ! is_numeric( $counter ) ) {
                    $new_name = $name . " 1";
                } else {
                    $new_name = substr( $name, 0, -1 ) . ((int) $counter + 1);
                }

                $name = $this->check_widget_area_name( $new_name );
            }
            echo esc_html( $name );
            exit();
        }

        public function save_widget_areas() {
            set_theme_mod( 'widget_areas', array_unique( $this->widget_areas ) );
        }

        /**
         * Register and display the custom widget_area areas we have set.
         */
        public function register_sidebars() {

            // Get widget areas
            if ( empty( $this->widget_areas ) ) {
                $this->widget_areas = $this->get_widget_areas();
            }

            // default_areainal widget areas is empty
            $this->default_area = array();

            // Save widget areas
            if ( ! empty( $this->default_area ) && $this->default_area != $this->widget_areas ) {
                $this->widget_areas = array_unique( array_merge( $this->widget_areas, $this->default_area ) );
                $this->save_widget_areas();
            }

            // If widget areas are defined add a sidebar area for each
            if ( is_array( $this->widget_areas ) ) {
                foreach ( array_unique( $this->widget_areas ) as $widget_area ) {
                    $args = array(

                        'name'			=> $widget_area,
                        'id'			=> sanitize_key( $widget_area ),
                        'class'			=> 'sassy-custom',
                        'description'   => sprintf( __( 'Custom Widget Area of the site - %s ', 'sassy' ), $widget_area ),
                        'before_widget' => '<section id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</section>',
                        'before_title'  => '<h2 class="widget-title">',
                        'after_title'   => '</h2>',
                    );
                    register_sidebar( $args );
                }
            }
        }

        /**
         * Return the widget_areas array.
         */
        public function get_widget_areas() {

            // If the single instance hasn't been set, set it now.
            if ( ! empty( $this->widget_areas ) ) {
                return $this->widget_areas;
            }

            // Get widget areas saved in theme mod
            $widget_areas = get_theme_mod( 'widget_areas' );

            // If theme mod isn't empty set to class widget area var
            if ( ! empty( $widget_areas ) && is_array( $widget_areas ) ) {
                $this->widget_areas = array_unique( array_merge( $this->widget_areas, $widget_areas ) );
            }

            // Return widget areas
            return $this->widget_areas;
        }

        /**
         * Before we create a new widget_area, verify it doesn't already exist. If it does, append a number to the name.
         */
        public function sassy_delete_widget_area() {
            // Check_ajax_reference('delete-sassy-widget_area-nonce');
            if ( ! empty( $_REQUEST['name'] ) ) {
                $name = strip_tags( ( stripslashes( $_REQUEST['name'] ) ) );
                $this->widget_areas = $this->get_widget_areas();
                $key = array_search($name, $this->widget_areas );
                if ( $key >= 0 ) {
                    unset( $this->widget_areas[$key] );
                    $this->save_widget_areas();
                }
                echo "widget_area-deleted";
            }
            die();
        }

        /**
         * Enqueue JS for the customizer controls
         */
        public function scripts() {

            // Load scripts and style
            wp_enqueue_script( 'sassy-widget-areas-script', THEME_URI . '/assets/back-end/js/widget-areas-script.js', array('jquery'), THEME_VERSION, true );

            // Get widgets
            $widgets = array();
            if ( ! empty( $this->widget_areas ) ) {
                foreach ( $this->widget_areas as $widget ) {
                    $widgets[$widget] = 1;
                }
            }

            // Localize script
            wp_localize_script(
                'sassy-widget-areas-script',
                'WidgetAttrLocalize',
                array(
                    'count'   => count( $this->default_area ),
                    'delete'  => esc_html__( 'Delete', 'sassy' ),
                    'confirm' => esc_html__( 'Confirm', 'sassy' ),
                    'cancel'  => esc_html__( 'Cancel', 'sassy' ),
                )
            );
        }

    }

    new Sassy_Custom_Sidebars();

}