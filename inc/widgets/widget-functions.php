<?php
/**
 * Sample implementation of the Custom widgets and sidebar feature
 *
 * @package Sassy
 */

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sassy_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'sassy' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'sassy' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    /* ---------------------------------------------
    # Footer Widget Areas
    ---------------------------------------------*/
    $activate_footer_widget_area    = get_theme_mod( 'sassy_footer_widgets_section_activate', true );
    $footer_widgets_layout          = get_theme_mod( 'sassy_footer_widgets_area_layout', 'footer-layout-8' );

    if ( true == $activate_footer_widget_area ) {
        if ($footer_widgets_layout == 'footer-layout-8' || $footer_widgets_layout == 'footer-layout-9' || $footer_widgets_layout == 'footer-layout-10') {
            $number_of_widgets = '4';
        } elseif ($footer_widgets_layout == 'footer-layout-5' || $footer_widgets_layout == 'footer-layout-6' || $footer_widgets_layout == 'footer-layout-7') {
            $number_of_widgets = '3';
        } elseif ($footer_widgets_layout == 'footer-layout-2' || $footer_widgets_layout == 'footer-layout-3' || $footer_widgets_layout == 'footer-layout-4') {
            $number_of_widgets = '2';
        } else {
            $number_of_widgets = '1';
        }

        for ($i = 1; $i <= $number_of_widgets; $i++) {
            register_sidebar(array(
                'name'          => sprintf( /* Translators: %d: widget number */
                    esc_html__('Footer %d', 'sassy'), $i),
                'id'            => 'footer-sidebar-' . $i,
                'description'   => sprintf( /* Translators: %d: widget number */
                    esc_html__('Add widgets in your footer widget area %d.', 'sassy'), $i),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ));
        }
    }
}
add_action( 'widgets_init', 'sassy_widgets_init' );










if ( ! function_exists( 'sassy_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see sassy_custom_header_setup().
	 */
	function sassy_header_style() {
		$header_text_color = get_header_textcolor();

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: add_theme_support( 'custom-header' ).
		 */
		if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
		?>
		<style type="text/css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
		?>
			.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
		<?php
			// If the user has set a custom color for the text use that.
			else :
		?>
			.site-title a,
			.site-description {
				color: #<?php echo esc_attr( $header_text_color ); ?>;
			}
		<?php endif; ?>
		</style>
		<?php
	}
endif;
