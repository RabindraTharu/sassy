<?php
/**
 * Sassy function for rendering meta-boxes in single post/page area
 *
 * @package Sassy
 */

/*----------------------------------------------------------------------
# Exit if accessed directly
-------------------------------------------------------------------------*/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/*----------------------------------------------------------------------
# Start Class Sassy_Post_Global_Meta_Box
-------------------------------------------------------------------------*/
if ( ! class_exists( 'Sassy_Post_Global_Meta_Box' ) ) {
    class Sassy_Post_Global_Meta_Box {

        private $post_types;

        /**
         * Construct Functions
         */
        public function __construct() {

            // Post types to add the meta-box to
            $this->post_types = array( 'post', 'page' );

            // Loop through post types and add meta-box to corresponding post types
            if ( $this->post_types ) {
                foreach( $this->post_types as $key => $val ) {
                    add_action( 'add_meta_boxes_'. $val, array( $this, 'post_meta' ), 11 );
                }
            }

            // Load scripts for the metabox
            add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );

            // Save meta Box
            add_action( 'save_post', array( $this, 'save_meta_data' ) );

        }

        /**
         * Add Meta-Box
         */
        public function post_meta( $post ) {

            // Add meta-box
            $obj = get_post_type_object( $post->post_type );
            add_meta_box(
                'post_meta_fields',
                $obj->labels->singular_name . ' '. esc_html__( 'Settings', 'sassy' ),
                array( $this, 'display_meta_box' ),
                $post->post_type,
                'normal',
                'high'
            );
        }

        /**
         * Enqueue scripts and styles
         */
        public function load_scripts( $hook ) {

            // Only needed on these admin screens
            if ( $hook != 'edit.php' && $hook != 'post.php' && $hook != 'post-new.php' ) {
                return;
            }

            // Get global post
            global $post;

            // Return if post is not object
            if ( ! is_object( $post ) ) {
                return;
            }

            // Return if wrong post type
            if ( ! in_array( $post->post_type, $this->post_types ) ) {
                return;
            }

            // Enqueue style
            wp_enqueue_style( 'sassy-admin-style', THEME_URI .'/assets/back-end/css/admin-style.css', false, THEME_VERSION, 'all' );

            // Image Uploader
            wp_enqueue_media();

            // Load jquery sortable
            wp_enqueue_script( 'jquery-ui-sortable' );

            // Enqueue Script
            wp_enqueue_script( 'sassy-image-uploader', THEME_URI . '/assets/back-end/js/image-uploader.js', array('jquery', 'jquery-ui-sortable'), THEME_VERSION, true );

            wp_enqueue_script( 'sassy-admin-script', THEME_URI . '/assets/back-end/js/admin-script.js', array('jquery'), THEME_VERSION, true );

            // Localize meta-box script
            wp_localize_script( 'sassy-admin-script', 'MetaboxAttr', array(
                'reset'     => esc_html__( 'Reset Settings', 'sassy' ),
                'cancel'    => esc_html__( 'Cancel Reset', 'sassy' ),
                'g_title'   => esc_html__( 'Add Images to Gallery', 'sassy' ),
                'g_button'  => esc_html__( 'Add to gallery', 'sassy' ),
                'g_remove'  => esc_html__( 'Remove image', 'sassy' ),
            ) );
        }

        /**
         * Display Meta-Box Fields
         */
        public function display_meta_box( $post ) {

            // Add nonce for security and authentication.
            wp_nonce_field( basename( __FILE__ ), 'post_metabox_nonce' );

            // Get current post data
            $post_id   = $post->ID;
            $post_type = get_post_type();

            // Get tabs
            $tabs = $this->meta_array( $post );

            // Empty notice
            $empty_notice = '<p>'. esc_html__( 'No meta settings available for this post type or user.', 'sassy' ) .'</p>';

            // Make sure tabs aren't empty
            if ( empty( $tabs ) ) {
                echo $empty_notice; return;
            }

            // Store tabs that should display on this specific page in an array for use later
            $active_tabs = array();
            foreach ( $tabs as $tab ) {
                $tab_post_type = isset( $tab['post_type'] ) ? $tab['post_type'] : '';
                if ( ! $tab_post_type ) {
                    $display_tab = true;
                } elseif ( in_array( $post_type, $tab_post_type ) ) {
                    $display_tab = true;
                } else {
                    $display_tab = false;
                }
                if ( $display_tab ) {
                    $active_tabs[] = $tab;
                }
            }

            // No active tabs
            if ( empty( $active_tabs ) ) {
                echo $empty_notice; return;
            }

            ?>

            <div class="metabox-container">
                <div class="metabox-settings-tabs">
                    <ul class="metabox-tab-nav">
                        <?php
                        // Output tab
                        $tab_count = '';
                        foreach ( $active_tabs as $tab ) {
                            $tab_count++;
                            // Define tab title
                            $tab_title = $tab['title'] ? $tab['title'] : esc_html__( 'Other', 'sassy' ); ?>
                            <li class="tab-link <?php if ( 1 == $tab_count ) { echo 'active'; }?>" data-tab="setting-tab-<?php echo esc_attr( $tab_count ); ?>"><?php echo esc_html( $tab_title ); ?></li>
                        <?php } ?>
                    </ul>

                    <div class="meta-box-wrap">
                        <?php
                        // Output tab sections
                        $section_count = '';
                        foreach ( $active_tabs as $tab ) {
                            $section_count++; ?>
                            <div id="setting-tab-<?php echo esc_attr( $section_count ); ?>" class="setting-tab <?php if ( 1 == $section_count ) { echo 'active'; }?>">
                                <?php
                                // Loop through sections and store meta output
                                foreach ( $tab['settings'] as $setting ) {
                                    // Vars
                                    $meta_id     = $setting['id'];
                                    $title       = $setting['title'];
                                    $type        = isset( $setting['type'] ) ? $setting['type'] : 'text';
                                    $default     = isset( $setting['default'] ) ? $setting['default'] : '';
                                    $meta_value  = get_post_meta( $post_id, $meta_id, true );
                                    $meta_value  = $meta_value ? $meta_value : $default; ?>


                                    <?php if ( 'text' == $type ) : ?>
                                        <section>
                                            <div class="input-holder">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">
                                                    <input id="<?php echo esc_attr( $meta_id ); ?>" class="input-w-480" type="text" name="<?php echo esc_attr( $meta_id ); ?>" value="<?php echo esc_attr( $meta_value ); ?>" />
                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>

                                    <?php elseif( 'link' == $type ) : ?>
                                        <section>
                                            <div class="input-holder">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">
                                                    <input id="<?php echo esc_attr( $meta_id ); ?>" class="input-w-480" type="url" name="<?php echo esc_attr( $meta_id ); ?>" value="<?php echo esc_url( $meta_value ); ?>" />
                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>

                                    <?php elseif( 'select' == $type ) : $options = isset ( $setting['options'] ) ? $setting['options'] : ''; ?>
                                        <section>
                                            <div class="input-holder">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">
                                                    <select id="<?php echo esc_attr( $meta_id ); ?>" name="<?php echo esc_attr( $meta_id ); ?>">
                                                        <?php foreach ( $options as $option_value => $option_name ) { ?>
                                                            <option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $meta_value, $option_value, true ); ?>><?php echo esc_html( $option_name ); ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>

                                    <?php elseif( 'radio' == $type ) : $options = isset ( $setting['options'] ) ? $setting['options'] : ''; ?>

                                        <section>
                                            <div class="input-holder">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">
                                                    <div id="specific-page-layout">
                                                        <fieldset>
                                                            <?php foreach ( $options as $option_value => $option_name ) : ?>
                                                                <input type="radio" name="<?php echo esc_attr( $meta_id ); ?>" id="has-<?php echo esc_attr( $option_value ); ?>" value="<?php echo esc_attr( $option_value ); ?>" <?php echo checked( $meta_value, $option_value, false ); ?>>
                                                                <label for="has-<?php echo esc_attr( $option_value ); ?>" class="has-<?php echo esc_attr( $option_value ); ?>"><?php echo esc_html( $option_name ); ?></label>
                                                            <?php endforeach; ?>
                                                        </fieldset>
                                                    </div>
                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>
                                    <?php elseif ( 'checkbox' == $type ) : $meta_value = ( 'on' != $meta_value ) ? false : true; ?>
                                        <section>
                                            <div class="input-holder-checkbox">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">
                                                    <input id="<?php echo esc_attr( $meta_id ); ?>" class="input-w-480" type="checkbox" name="<?php echo esc_attr( $meta_id ); ?>" <?php checked( $meta_value, true, true ); ?>/>
                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>
                                    <?php elseif ( 'multi-image' == $type ) :
                                        $array_value    = $meta_value;
                                        $attachments    = array_filter( explode( ',', $array_value ) );?>
                                        <section>
                                            <div class="input-holder-checkbox">
                                                <?php if( $title ) : ?>
                                                    <div class="input-label">
                                                        <label for="<?php echo esc_attr( $meta_id ); ?>"><?php echo esc_html( $title ); ?></label>
                                                    </div><!-- .input-field -->
                                                <?php endif; ?>
                                                <div class="input-field">

                                                    <div id="metabox_gallery_container">
                                                        <ul class="metabox_gallery_images">
                                                            <?php
                                                            if ( $attachments ) {
                                                                foreach ( $attachments as $attachment_id ) {
                                                                    if ( wp_attachment_is_image ( $attachment_id  ) ) { ?>
                                                                        <li class="image" data-attachment_id="<?php echo esc_attr($attachment_id); ?>">
                                                                            <div class="thumbnail"><?php echo wp_get_attachment_image( $attachment_id, 'thumbnail' );?></div>
                                                                            <a href="#" class="sassy-gallery-image-remove" title="<?php esc_html_e('Remove Image','sassy');?>">X</a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            } ?>
                                                        </ul>
                                                        <input type="hidden" id="<?php echo esc_attr( $meta_id ); ?>" name="<?php echo esc_attr( $meta_id ); ?>" value="<?php echo esc_attr( $array_value ); ?>" />
                                                    </div>
                                                    <p class="sassy_add_gallery_images hide-if-no-js">
                                                        <a href="#" class="button-primary"><?php esc_html_e( 'Add/Edit Images', 'sassy' ); ?></a>
                                                    </p>

                                                </div><!-- .input-field -->
                                            </div><!-- .input-holder -->
                                        </section>
                                    <?php endif; ?>
                                <?php } ?>

                            </div>
                        <?php } ?>

                    </div>
                </div>

                <div class="metabox-reset-settings">
                    <a class="button button-secondary metabox-reset-btn"><?php esc_html_e( 'Reset Settings', 'sassy' ); ?></a>
                    <div class="metabox-reset-checkbox"><input type="checkbox" name="metabox-value-reset"> <?php esc_html_e( 'Are you sure? Check this box, then update your post to reset all settings.', 'sassy' ); ?></div>
                </div>

            </div>

            <?php
        }

        /**
         * Save Meta-Box Values
         */
        public function save_meta_data( $post_id ) {

            // Verify that the nonce is valid.
            if ( ! isset( $_POST['post_metabox_nonce'] ) || ! wp_verify_nonce( $_POST['post_metabox_nonce'], basename( __FILE__ ) ) ) {
                return;
            }

            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
            }

            // Check the user's permissions.
            if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) ) {
                    return;
                }

            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) ) {
                    return;
                }
            }

            /* OK, it's safe for us to save the data now. Now we can loop through fields */

            // Check reset field
            $reset = isset( $_POST['metabox-value-reset'] ) ? $_POST['metabox-value-reset'] : '';

            // Get array of settings to save
            $tabs = $this->meta_array();
            $settings = array();
            foreach( $tabs as $tab ) {
                foreach ( $tab['settings'] as $setting ) {
                    $settings[] = $setting;
                }
            }

            // Loop through settings and validate
            foreach ( $settings as $setting ) {

                // Vars
                $value = '';
                $id    = $setting['id'];
                $type  = isset ( $setting['type'] ) ? $setting['type'] : 'text';

                // Make sure field exists and if so validate the data
                if ( isset( $_POST[$id] ) ) {

                    // Validate text
                    if ( 'text' == $type ) {
                        $value = sanitize_text_field( $_POST[$id] );
                    }
                    // Links
                    elseif ( 'link' == $type ) {
                        $value = esc_url_raw( $_POST[$id] );
                    }
                    // Validate select
                    elseif ( 'select' == $type ) {
                        if ( 'default' == $_POST[$id] ) {
                            $value = '';
                        } else {
                            $value = sanitize_text_field( $_POST[$id] );
                        }
                    }
                    // Validate radio
                    elseif ( 'radio' == $type ) {
                        if ( 'default' == $_POST[$id] ) {
                            $value = '';
                        } else {
                            $value = sanitize_text_field( $_POST[$id] );
                        }
                    }
                    // Multi Images
                    elseif ( 'multi-image' == $type ) {
                        $value = sanitize_text_field( $_POST[$id] );
                        // Turn comma separated values into array
                        $value = explode( ',', $value );
                        // Clean the array
                        $value = array_filter( $value  );
                        // Return back to comma separated list with no trailing comma. This is common when deleting the images
                        $value =  implode( ',', $value );
                    }
                    // All else
                    else {
                        $value = $_POST[$id];
                    }

                    // Update meta if value exists
                    if ( $value && 'on' != $reset ) {
                        update_post_meta( $post_id, $id, $value );
                    }

                    // Otherwise cleanup stuff
                    else {
                        delete_post_meta( $post_id, $id );
                    }
                }

            }

        }

        /**
         * Settings Array
         */
        private function meta_array( $post = null ) {

            // Prefix
            $prefix = 'sassy_';

            // Define array
            $array = array();

            // Default variable
            $default = esc_html__( 'Default', 'sassy' );

            // Main Tab
            $array['main']  = array(
                'title'     => esc_html__( 'Main', 'sassy' ),
                'settings'  => array(
                    'content_layout'    =>array(
                        'title'         => esc_html__( 'Content Layout', 'sassy' ),
                        'id'            => $prefix . 'content_layout',
                        'type'          => 'radio',
                        'options'       => sassy_content_layouts( array( 'default-sidebar' => $default )),
                        'default'       => 'default-sidebar',
                    ),
                    'sidebar_layout'     => array(
                        'title'         => esc_html__( 'Select Sidebar', 'sassy' ),
                        'id'            => $prefix . 'sidebar_layout',
                        'type'          => 'select',
                        'options'       => sassy_available_sidebars( array( 'default-sidebar' => $default ) ),
                    ),
                    'breadcrumb'     => array(
                        'title'         => esc_html__( 'Breadcrumb', 'sassy' ),
                        'id'            => $prefix . 'breadcrumb',
                        'type'          => 'select',
                        'options'       => sassy_enable_elements( array( '' => $default ) ),
                    ),
                    'social_share'     => array(
                        'title'         => esc_html__( 'Social Share', 'sassy' ),
                        'id'            => $prefix . 'social_share',
                        'type'          => 'select',
                        'options'       => sassy_enable_elements( array( '' => $default ) ),
                    ),
                ),
            );

            // Sub Title Tab
            $array['sub_title']   = array(
                'title'             => esc_html__( 'Sub Title', 'sassy' ),
                'settings'          => array(
                    'subtitle'   => array(
                        'title'         => esc_html__( 'Sub Heading Text', 'sassy' ),
                        'id'            => $prefix . 'subtitle',
                        'type'          => 'text',
                    ),
                ),
            );

            // Gallery Tab
            $array['gallery']   = array(
                'title'                     => esc_html__( 'Gallery', 'sassy' ),
                'settings'                  => array(
                    'gallery_images'        => array(
                        'title'             => esc_html__( 'Images', 'sassy' ),
                        'id'                => $prefix . 'gl_images',
                        'type'              => 'multi-image',
                    ),
                    'gallery_image_ratio'   => array(
                        'title'             => esc_html__( 'Image Ratio', 'sassy' ),
                        'id'                => $prefix . 'gl_image_ratio',
                        'type'              => 'select',
                        'options'           => sassy_gallery_image_ratio(),
                        'default'           => '4x3',
                    ),
                    'gallery_image_per_col' => array(
                        'title'             => esc_html__( 'Columns Per Row', 'sassy' ),
                        'id'                => $prefix . 'gl_image_per_row',
                        'type'              => 'select',
                        'options'           => sassy_col_per_row(),
                        'default'           => 'col-4',
                    ),
                    'gallery_image_position' => array(
                        'title'             => esc_html__( 'Position', 'sassy' ),
                        'id'                => $prefix . 'gl_position',
                        'type'              => 'select',
                        'options'           => sassy_gallery_position(),
                        'default'           => 'after-content',
                    ),
                ),
            );

            // Redirect Title Tab
            $array['redirect_url']   = array(
                'title'             => esc_html__( 'Redirect URL', 'sassy' ),
                'settings'          => array(
                    'subtitle'   => array(
                        'title'         => esc_html__( 'Custom Link', 'sassy' ),
                        'id'            => $prefix . 'redirect_url',
                        'type'          => 'link',
                    ),
                ),
            );

            // Apply filter & return settings array
            return apply_filters( 'sassy_metabox_array', $array, $post );
        }
    }

    new Sassy_Post_Global_Meta_Box;
}

/* ---------------------------------------------
# Start Class Sassy_Secondary_Image_Meta_Box
---------------------------------------------*/
if ( ! class_exists( 'Sassy_Secondary_Image_Meta_Box' ) ) {
    class Sassy_Secondary_Image_Meta_Box {

        private $post_types;

        /**
         * Construct Functions
         */
        public function __construct() {

            // Post types to add the meta-box to
            $this->post_types = array( 'post' );

            // Loop through post types and add meta-box to corresponding post types
            if ( $this->post_types ) {
                foreach( $this->post_types as $key => $val ) {
                    add_action( 'add_meta_boxes_'. $val, array( $this, 'post_meta' ), 11 );
                }
            }

            // Save meta
            add_action( 'save_post', array( $this, 'save_meta_data' ) );

        }

        /**
         * Add Meta-Box
         */
        public function post_meta( $post ) {

            add_meta_box(
                'post_secondary_image_meta_field',
                esc_html__( 'Secondary Image', 'sassy' ),
                array( $this, 'display_meta_box' ),
                $post->post_type,
                'side',
                'low'
            );
        }

        /**
         * Display Meta-Box Fields
         */
        public function display_meta_box( $post ) {

            // Add nonce for security and authentication.
            wp_nonce_field( basename( __FILE__ ), 'post_secondary_image_metabox_nonce' );

            // Retrieve an existing value from the database.
            $post_secondary_image_id      = get_post_meta($post->ID, 'post_secondary_image_id', true);

            // Set default values.
            if( empty( $post_secondary_image_id ) ) $post_secondary_image_id  = '';
            ?>

            <div class="meta-box-wrap">
                <section>
                    <div class="input-holder">
                        <div class="input-field">
                            <div class="media-uploader">
                                <div class="custom_media_preview">
                                    <?php if ( '' !== $post_secondary_image_id ) :
                                        $image_src = wp_get_attachment_image_src( $post_secondary_image_id, 'full' ); ?>
                                        <span class="delete_media_image">x</span>
                                        <img class="custom_media_preview_default" src="<?php echo esc_url( $image_src[0] ); ?>" style="max-width:100%;" />
                                    <?php endif; ?>
                                </div>
                                <input type="hidden" class="widefat custom_media_input" name="post_secondary_image_id" value="<?php if ( $post_secondary_image_id !== '' ) echo esc_attr( $post_secondary_image_id ); ?>" style="margin-top:5px;" />
                                <a href="#" class="custom_media_upload" data-choose="<?php esc_attr_e( 'Choose an image', 'sassy' ); ?>" data-update="<?php esc_attr_e( 'Use image', 'sassy' ); ?>" ><?php esc_html_e( 'Set featured image', 'sassy' ); ?></a>
                            </div>
                        </div><!-- .input-field -->
                    </div><!-- .input-holder -->
                </section>
            </div>

            <?php
        }

        /**
         * Save Meta-Box Value
         */
        public function save_meta_data( $post_id ) {

            // Verify that the nonce is valid.
            if ( ! isset( $_POST['post_secondary_image_metabox_nonce'] ) || ! wp_verify_nonce( $_POST['post_secondary_image_metabox_nonce'], basename( __FILE__ ) ) ) {
                return;
            }

            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
            }

            // Check the user's permissions.
            if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) ) {
                    return;
                }

            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) ) {
                    return;
                }
            }

            // Secondary Featured Image
            if( isset( $_POST['post_secondary_image_id'] ) ) {
                update_post_meta( $post_id, 'post_secondary_image_id', absint( $_POST['post_secondary_image_id'] ) );
            } else {
                delete_post_meta( $post_id, 'post_secondary_image_id' );
            }

        }
    }
    new Sassy_Secondary_Image_Meta_Box;
}
