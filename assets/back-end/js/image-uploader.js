/**
 * File image-gallery.js.
 *
 * Theme Custom Image Gallery script.
 */
( function( $ ) {

    'use strict';

    $( document ).on( 'ready', function() {

        // Uploading files
        var image_gallery_frame;
        var $image_gallery_ids   = $( '#sassy_gl_images' );
        var $sassy_gallery_images = $( '#metabox_gallery_container ul.metabox_gallery_images' );

        jQuery( '.sassy_add_gallery_images' ).on( 'click', 'a', function( event ) {
            var $el = $( this );
            var attachment_ids = $image_gallery_ids.val();
            event.preventDefault();

            // If the media frame already exists, reopen it.
            if ( image_gallery_frame ) {
                image_gallery_frame.open();
                return;
            }

            // Create the media frame.
            image_gallery_frame = wp.media.frames.downloadable_file = wp.media( {

                // Set the title of the modal.
                title    : MetaboxAttr.g_title,
                button   : {
                    text : MetaboxAttr.g_button,
                },
                multiple: true

            } );

            // When an image is selected, run a callback.
            image_gallery_frame.on( 'select', function() {
                var selection = image_gallery_frame.state().get('selection');
                selection.map( function( attachment ) {
                    attachment = attachment.toJSON();
                    if ( attachment.id ) {
                        attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;
                        $sassy_gallery_images.append('\
							<li class="image" data-attachment_id="' + attachment.id + '">\
									<div class="thumbnail">\
										<img src="' + attachment.url + '" />\
								   <a href="#" class="sassy-gallery-image-remove" title="'+ MetaboxAttr.g_remove +'">X</a>\
								</div>\
							</li>');
                    }
                } );
                $image_gallery_ids.val( attachment_ids );

            } );

            // Finally, open the modal.
            image_gallery_frame.open();

        } );

        // Image ordering
        $sassy_gallery_images.sortable( {
            items                : 'li.image',
            cursor               : 'move',
            scrollSensitivity    : 40,
            forcePlaceholderSize : true,
            forceHelperSize      : false,
            helper               : 'clone',
            opacity              : 0.65,
            placeholder          : 'sassy-metabox-shortable-placeholder',
            start                : function( event,ui ) {
                ui.item.css( 'background-color', '#f6f6f6' );
            },
            stop                 : function( event,ui ) {
                ui.item.removeAttr( 'style' );
            },
            update               : function( event, ui ) {
                var attachment_ids = '';
                $( '#metabox_gallery_container ul li.image' ).css( 'cursor', 'default' ).each( function() {
                    var attachment_id = jQuery(this).attr( 'data-attachment_id' );
                    attachment_ids = attachment_ids + attachment_id + ',';
                } );
                $image_gallery_ids.val( attachment_ids );
            }
        } );

        // Remove images
        $( '#metabox_gallery_container' ).on( 'click', 'a.sassy-gallery-image-remove', function() {
            $( this ).closest( 'li.image' ).remove();
            var attachment_ids = '';
            $( '#metabox_gallery_container ul li.image' ).css( 'cursor', 'default' ).each( function() {
                var attachment_id = jQuery( this ).attr( 'data-attachment_id' );
                attachment_ids = attachment_ids + attachment_id + ',';
            } );
            $image_gallery_ids.val( attachment_ids );
            return false;
        } );


        // Single Image Uploader
        var file_frame;

        $( document.body ).on( 'click', '.custom_media_upload', function( event ) {
            var $el = $( this );

            var file_target_input   = $el.parent().find( '.custom_media_input' );
            var file_target_preview = $el.parent().find( '.custom_media_preview' );

            event.preventDefault();

            // Create the media frame.
            file_frame = wp.media.frames.media_file = wp.media({
                // Set the title of the modal.
                title: $el.data( 'choose' ),
                button: {
                    text: $el.data( 'update' )
                },
                states: [
                    new wp.media.controller.Library({
                        title: $el.data( 'choose' ),
                        library: wp.media.query({ type: 'image' })
                    })
                ]
            });

            // When an image is selected, run a callback.
            file_frame.on( 'select', function() {
                // Get the attachment from the modal frame.
                var attachment = file_frame.state().get( 'selection' ).first().toJSON();

                // Initialize input and preview change.
                if ( attachment.id ) {
                    file_target_input.val( attachment.id ).trigger('change');
                    file_target_preview.css({ display: 'none' }).find( 'img' ).remove();
                    file_target_preview.css({ display: 'block' }).append( '<img src="' + attachment.url + '" style="max-width:100%">' );
                }

            });

            // Finally, open the modal.
            file_frame.open();
        });

        // Remove Media Preview
        $( document.body ).on( 'click', '.delete_media_image', function(){
            var $el = $( this ).closest( '.media-uploader' );
            $el.find( '.custom_media_input' ).val( '' ).trigger('change');
            $el.find( '.custom_media_preview' ).css({ display: 'none' }).find( 'img' ).remove();

            return false;
        });

    } );

} ) ( jQuery );

